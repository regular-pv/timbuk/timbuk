open Collections
open CodeMap
open Timbuk
open TimbukSolving
open TimbukSpec
open Spec
open Unicode
open UString

let authors = [
  "Timothée Haudebourg <timothee.haudebourg@irisa.fr>"
]

let version = "4.0"

let summary =
  "Process the given Timbuk Specification files."

module Conf = struct
  type t = {
    verbosity: int;
    filenames: string list;
    refine_constants: bool;
    typing_only: bool;
    embedded: bool;
    abstraction_details: bool
    (* log_smt: bool *)
  }

  let default = {
    verbosity = 2;
    filenames = [];
    refine_constants = true;
    typing_only = false;
    embedded=false;
    abstraction_details=true
    (* log_smt = false *)
  }

  let verbose t =
    { t with verbosity = t.verbosity+1 }

  let quiet t =
    { t with verbosity = t.verbosity-1 }

  let typing_only t =
    { t with typing_only = true }

  let embedded t=
    {t with embedded=true}
  let no_verbose t=
    {t with verbosity=0}

  let no_abstraction_details t=
    {t with abstraction_details=false}

  let no_refine_constants t =
    { t with refine_constants = false }

  (* let log t =
     { t with log_smt = true } *)

  let log_level t =
    match t.verbosity with
    | _ when t.verbosity <= 0 -> None
    | 1 -> Some Logs.App
    | 2 -> Some Logs.Error
    | 3 -> Some Logs.Warning
    | 4 -> Some Logs.Info
    | _ -> Some Logs.Debug

  let add_file filename t =
    { t with filenames = t.filenames@[filename] }
end

module Integer = struct
  type t = int

  let compare a b = a - b

  let print i fmt =
    Format.fprintf fmt "%d" i
end

module Cvc4Conf = struct
  let path () = "cvc4"

  let count = ref 0

  let log () =
    (* let i = !count in
       count := i + 1;
       let filename = "log/cvc4_" ^ (string_of_int i) ^ ".smt2" in
       Some (filename, open_out filename) *)
    None
end

(* Polymorphic typing. *)
module LocPolyType = struct
  type t = PolyType.t * CodeMap.Span.t
  let compare (a, _) (b, _) = PolyType.compare a b
  let equal (a, _) (b, _) = PolyType.equal a b
  let product (a, span) (b, _) =
    match PolyType.product a b with
    | Some t -> Some (t, span)
    | None -> None
  let hash (t, _) = PolyType.hash t
  let print (t, _) fmt = PolyType.print t fmt
end
module PolyTypedPattern = TypedPattern.Make (Aut.Sym) (Pattern.Var) (LocPolyType)
module PolyTypedRule = Timbuk.Rule.Make (PolyTypedPattern)
module PolyTypedTrs = Relation.Make (PolyTypedPattern) (PolyTypedRule)
module PolyTyper = TimbukTyping.Poly.Make (CodeMap.Span) (Symbol) (Base) (Aut) (LocPattern) (LocTrs) (PolyTypedTrs)

(* Monomorphic typing. *)
module MonoSym = struct
  type t = Aut.Sym.t * MonoType.t list * MonoType.t
  let compare (fa, la, ta) (fb, lb, tb) =
    let c = Aut.Sym.compare fa fb in
    if c = 0 then
      let c = MonoType.compare ta tb in
      if c = 0 then List.compare MonoType.compare la lb else c
    else c
  let equal (fa, la, ta) (fb, lb, tb) = Aut.Sym.equal fa fb && MonoType.equal ta tb && List.equal MonoType.equal la lb
  let arity (f, _, _) = Aut.Sym.arity f
  let hash = Hashtbl.hash
  let print (f, _, _) fmt = Aut.Sym.print f fmt
  (* let print (f, l, ty) fmt =
     Format.fprintf fmt "[%t:%t:%t]" (Aut.Sym.print f) (List.print MonoType.print "," l) (MonoType.print ty) *)
end
module MonoTerm = Timbuk.Term.Make (MonoSym)
module LocMonoType = struct
  type t = MonoType.t * CodeMap.Span.t
  let compare (a, _) (b, _) = MonoType.compare a b
  let equal (a, _) (b, _) = MonoType.equal a b
  let product (a, span) (b, _) =
    match MonoType.product a b with
    | Some t -> Some (t, span)
    | None -> None
  let hash (t, _) = MonoType.hash t
  let print (t, _) fmt = MonoType.print t fmt
end
module MonoAut = Automaton.Make (MonoSym) (MonoType) (Aut.Label)
module MonoTypedPattern = TypedPattern.Make (MonoSym) (Pattern.Var) (LocMonoType)
module MonoTypedRule = Timbuk.Rule.Make (MonoTypedPattern)
module MonoTypedTrs = Relation.Make (MonoTypedPattern) (MonoTypedRule)
module MonoTyper = TimbukTyping.Mono.Make (CodeMap.Span) (Symbol) (MonoType) (PolyType) (Aut) (PolyTypedPattern) (PolyTypedTrs) (MonoAut) (MonoTypedPattern) (MonoTypedTrs)

(* Subtype typer. *)
module SubTyper = TimbukTyping.Sub.Make (CodeMap.Span) (MonoAut) (MonoTypedPattern) (MonoTypedTrs)

(* Regular typing. *)
module Solver = Cvc4.Make (Cvc4Conf) (MonoAut.State)
module AbsSolver = Cvc4.Make (Cvc4Conf) (Integer)
module TypeSystem = TimbukTyping.RefinedTypeSystem.Make (MonoAut)
module RegularTyper = TimbukTyping.Regular.Make (CodeMap.Span) (TypeSystem) (MonoTypedPattern) (MonoTypedTrs) (Solver) (AbsSolver)
module RegularType = RegularTyper.RegularType
module FixpointLearner = TimbukFixpointLearning.Completion.Make
    (CodeMap.Span)
    (TypeSystem)
    (RegularTyper.RegularTypePartition)
    (RegularTyper.AbsType)
    (RegularTyper.AbsTypedPattern)
    (RegularTyper.AbsTypedTrs)
    (RegularTyper.SolvedAut)
    (AbsSolver)

let spec =
  let open Clap in
  app "Timbuk regular typing" version authors summary
  +> arg (short 'v') (Flag Conf.verbose) "Increase the level of verbosity."
  +> arg (short 'q') (Flag Conf.quiet) "Decrease the level of verbosity."
  +> arg (long "typing-only") (Flag Conf.typing_only) "Stop after the monomorphic typing phase."
  +> arg (long "no-refine-consts") (Flag Conf.no_refine_constants) "Do not refine constants."
  (* +> arg (id "log" 'l') (Flag Conf.log) "Log SMT-solver instances." *)
  +> anon "FILE" ~multiple:true (String Conf.add_file) "Sets the input file(s) to process."

let rec instanciate_pattern aut = function
  | RegularTyper.RegularTypedPattern.Cons (f, l), _ ->
    MonoTerm.Term (f, List.map (instanciate_pattern aut) l)
  | RegularTyper.RegularTypedPattern.Var x, (ty, _) ->
    let ty = MonoAut.StateSet.choose ty in
    MonoAut.BoundTerm.strip (MonoAut.pick_term_in ty aut)
  | RegularTyper.RegularTypedPattern.Cast pattern, _ ->
    instanciate_pattern aut pattern

type runtime_error =
  | IrreducibleNonValue of MonoTerm.t
  | InvalidTargetType of MonoAut.StateSet.t * RegularTyper.RegularTypePartition.t
  | RegularTypeMissmatch of RegularTyper.RegularType.t * RegularTyper.RegularTypedPattern.t * MonoAut.t

type error =
  | Parse of Parse.error * CodeMap.Span.t
  | Build of Build.error * CodeMap.Span.t
  | PolyType of PolyTyper.error * CodeMap.Span.t
  | MonoType of MonoTyper.error * CodeMap.Span.t
  | SubType of SubTyper.error * CodeMap.Span.t
  | RegularType of RegularTyper.error
  | Runtime of runtime_error

exception Error of error

let error_kind = function
  | Parse _ -> "syntax error"
  | Build (e, _) -> "build error"
  | PolyType (e, _) -> "polymorphic type error"
  | MonoType (e, _) -> "monomorphic type error"
  | SubType (e, _) -> "sub-type error"
  | RegularType _ -> "regular type error"
  | Runtime (RegularTypeMissmatch _) -> "found counter-example"
  | Runtime e -> "runtime error"

let error_message = function
  | Parse _ -> None
  | Build (e, _) -> Some (Format.asprintf "%t" (Build.print_error e))
  | PolyType (e, _) -> Some (Format.asprintf "%t" (PolyTyper.print_error e))
  | MonoType (e, _) -> Some (Format.asprintf "%t" (MonoTyper.print_error e))
  | SubType (e, _) -> Some (Format.asprintf "%t" (SubTyper.print_error e))
  | RegularType (RegularTyper.BiTypedTerm (_, q, q', _)) ->
    Some (Format.asprintf "unable to separate types `%t' and `%t'" (RegularTyper.TypeAut.State.print q) (RegularTyper.TypeAut.State.print q'))
  | Runtime (IrreducibleNonValue term) ->
    Some (Format.asprintf "the following term is irreduce but is not a value:\n%t" (MonoTerm.print term))
  | Runtime (InvalidTargetType (target_ty, partition)) ->
    let print_partition_elt elt fmt =
      Format.fprintf fmt "{%t}" (MonoAut.StateSet.print MonoType.print ", " elt)
    in
    Some (Format.asprintf "the given target type `%t` is not in the type partition `{%t}'"
            (RegularType.print target_ty)
            (RegularTyper.RegularTypePartition.print print_partition_elt ", " partition))
  | Runtime (RegularTypeMissmatch (expected_ty, (_, (found_ty, _)), _)) ->
    Some (Format.asprintf "expected regular type `%t', found `%t'" (RegularType.print expected_ty) (RegularType.print found_ty))

let error_content = function
  | Parse (_, span) -> (None, Some span)
  | Build (e, span) -> (Build.error_label e, Some span)
  | PolyType (e, span) -> (PolyTyper.error_label e, Some span)
  | MonoType (e, span) -> (MonoTyper.error_label e, Some span)
  | SubType (e, span) -> (SubTyper.error_label e, Some span)
  | RegularType _ -> (None, None)
  | Runtime _ -> (None, None)

let format_error_hints fmt = function
  | Parse _ -> ()
  | Build (e, _) -> Build.format_error_hints e fmt
  | PolyType (e, _) ->
    begin
      match e with
      | TypeMissmatch (expected_ty, Some expected_span, _) ->
        let msg = Format.asprintf "type `%t' is required here" (Aut.State.print expected_ty) in
        CodeMap.Formatter.add fmt expected_span (Some msg) CodeMap.Formatter.Highlight.Note
      | _ -> ()
    end
  | MonoType (_e, _) -> ()
  | SubType (_e, _) -> ()
  | RegularType _ -> ()
  | Runtime _ -> ()

let help ppf f =
  Format.fprintf ppf "\x1b[1;34mnote: \x1b[0m";
  f (Format.fprintf ppf);
  Format.fprintf ppf "@."

let print_error_help ppf = function
  | Runtime (RegularTypeMissmatch (expected_ty, found_pattern, aut)) ->
    let sample = instanciate_pattern aut found_pattern in
    help ppf (fun m -> m "here is a term that may not rewrite to %t:\n\n      \x1b[1;1m%t\x1b[0m\n" (RegularType.print expected_ty) (MonoTerm.print sample));
  | _ -> ()

let format_error input e ppf =
  let label_opt, span_opt = error_content e in
  let msg_opt = error_message e in
  Format.fprintf ppf "\x1b[1;31m%s\x1b[0m\x1b[1;1m" (error_kind e);
  begin match span_opt with
    | Some span -> Format.fprintf ppf " (%t)" (CodeMap.Span.format span)
    | None -> ()
  end;
  begin match msg_opt with
    | Some msg -> Format.fprintf ppf ": %s" msg
    | None -> ()
  end;
  Format.fprintf ppf "\x1b[0m@.";
  begin match span_opt with
    | Some span ->
      let fmt = Formatter.create () in
      let viewport = Span.from_start (Span.aligned ~margin:1 span) in
      Formatter.add fmt span label_opt Formatter.Highlight.Error;
      format_error_hints fmt e;
      Formatter.print fmt input viewport stderr
    | None -> ()
  end;
  print_error_help ppf e

let state_factory _ =
  TimbukTyping.GenericTypes.Base (Base.create ())

let constant_refiner conf =
  (state_factory conf), (), ()

let type_of (_, (ty, _)) = ty

(* Check if a symbol is a functional symbol. *)
let is_functional values_abstraction f ty =
  let confs = MonoAut.configurations_for_state ty values_abstraction in
  MonoAut.LabeledConfigurationSet.for_all (
    function (conf, _) ->
    match conf with
    | MonoAut.Configuration.Cons (f', _) ->
      not (MonoAut.Sym.equal f f')
    | _ -> true
  ) confs

let is_functional_conf values_abstraction ty = function
  | MonoAut.Configuration.Cons (f, _) -> is_functional values_abstraction f ty
  | _ -> false

(*
(* Find human readable names for newly generated states *)
let human_readable_names mono_tc values_abstraction abstraction =
  let is_human_state q = true
  in
  0
*)

let format_abstraction values_abstraction abstraction fmt =
  let non_functional_lines, functional_lines = MonoAut.fold_transitions (
      fun conf _ ty (nfl, fl) ->
        let line = Format.asprintf "%t -> %t" (MonoAut.Configuration.print conf) (MonoAut.State.print ty) in
        if is_functional_conf values_abstraction ty conf then
          (nfl, line::fl)
        else
          (line::nfl, fl)
    ) abstraction ([], [])
  in
  let margin = (List.fold_right (fun line m -> max m (String.length line)) non_functional_lines 0) + 4 in
  let rec fold f l1 l2 x =
    match l1, l2 with
    | a::l1, b::l2 -> fold f l1 l2 (f a b x)
    | a::l1, [] -> fold f l1 [] (f a "" x)
    | [], b::l2 -> fold f [] l2 (f "" b x)
    | [], [] -> x
  in
  let rec pad line =
    if String.length line >= margin then line else
      pad (Utf8String.push (UChar.of_ascii ' ') line)
  in
  Format.fprintf fmt "%s%s\n\n" (pad "values abstraction:") "TRS abstraction:";
  fold (
    fun nfl fl () ->
      let nfl = pad nfl in
      Format.fprintf fmt "%s%s\n" nfl fl
  ) non_functional_lines functional_lines ()

let type_abstraction ty abs =
  let state_just_for conf abs =
    let states = MonoAut.states_for_configuration conf abs in
    let just_for_ty (q, _) =
      Logs.debug (fun m -> m "is %t just for %t?" (MonoAut.State.print q) (MonoAut.Configuration.print conf));
      match MonoAut.LabeledConfigurationSet.cardinal (MonoAut.configurations_for_state q abs) with
      | 1 ->
        Logs.debug (fun m -> m "yes.");
        true
      | _ ->
        Logs.debug (fun m -> m "no.");
        false
    in
    match MonoAut.LabeledStateSet.search_opt just_for_ty states with
    | Some (q, _) -> q, abs
    | None ->
      let q = state_factory () in
      q, MonoAut.add_transition conf () q abs
  in
  let confs = MonoAut.configurations_for_state ty abs in
  MonoAut.LabeledConfigurationSet.fold (
    fun (conf, _) (types, partition, abs) ->
      match conf with
      | MonoAut.Configuration.Cons (_, []) ->
        let q, abs = state_just_for conf abs in
        TypeSystem.TypeSet.add q types, RegularTyper.RegularTypePartition.add (MonoAut.StateSet.singleton q) partition, abs
      | MonoAut.Configuration.Cons (_, _) ->
        raise (Invalid_argument "Target type must be a finite set of constants.")
      | MonoAut.Configuration.Var _ -> types, partition, abs
  ) confs (TypeSystem.TypeSet.empty, RegularTyper.RegularTypePartition.empty, abs)

let process_spec conf spec =
  (* let open Log in *)
  let trs = match Trss.find_opt "R" spec.spec_trss with
    | Some (trs, _) -> trs
    | None ->
      Logs.err (fun m -> m "No TRS `R` defined.");
      exit 1
  in
  let patterns, tc = match PatternSets.find_opt "ToType" spec.spec_pattern_sets with
    | Some ((patterns, Some tc), _) ->
      patterns, tc
    | Some ((_, None), _) ->
      Logs.err (fun m -> m "No type system.");
      exit 1
    | None ->
      Logs.err (fun m -> m "No patterns `ToType` defined.");
      exit 1
  in

  (* Polymorphic typing phase *)
  let poly_typer = PolyTyper.create trs tc in
  let poly_typed_trs = PolyTyper.typed_trs poly_typer in
  Logs.info (fun m -> m "polymorphic TRS:\n%t" (PolyTypedTrs.print poly_typed_trs));

  let constant_refiner = match conf.Conf.refine_constants with
    | true -> Some constant_refiner
    | false -> None
  in

  ignore (LocPatternSet.fold (
      fun (opt_typed_pattern, target_ty_opt) poly_typer ->
        let poly_typed_pattern, poly_typer = PolyTyper.type_pattern opt_typed_pattern poly_typer in
        let tc = PolyTyper.type_system poly_typer in
        Logs.info (fun m -> m "poly-typed automaton:\n%t" (Aut.print tc));
        Logs.info (fun m -> m "poly-typed pattern: %t" (PolyTypedPattern.print poly_typed_pattern));

        let mono_typer = MonoTyper.create ?constant_refiner poly_typed_trs tc in
        (* let mono_typer = MonoTyper.create poly_typed_trs tc in *)
        let mono_typed_pattern, mono_typer = MonoTyper.type_pattern poly_typed_pattern mono_typer in

        Logs.info (fun m -> m "mono-typed pattern: %t" (MonoTypedPattern.print mono_typed_pattern));

        let mono_typed_trs = MonoTyper.typed_trs mono_typer in
        Logs.info (fun m -> m "mono-typed TRS:\n%t" (MonoTypedTrs.print mono_typed_trs));

        let mono_tc = MonoTyper.type_system mono_typer in
        Logs.info (fun m -> m "mono-typed automaton:\n%t" (MonoAut.print mono_tc));

        let sub_typer = SubTyper.create mono_tc mono_typed_trs in
        let sub_typed_patterns, sub_typer = SubTyper.type_pattern mono_typed_pattern sub_typer in
        let sub_typed_trs = SubTyper.typed_trs sub_typer in
        Logs.info (fun m -> m "sub-typed TRS:\n%t" (MonoTypedTrs.print sub_typed_trs));

        List.iter (
          function sub_typed_pattern ->
            Logs.info (fun m -> m "sub-typed pattern: %t" (MonoTypedPattern.print sub_typed_pattern));

            if not conf.Conf.typing_only then begin
              let types, ty_precision, abstraction = type_abstraction (type_of sub_typed_pattern) mono_tc in

              (* check regular type annotation *)
              let mono_target_ty_opt = match target_ty_opt with
                | Some target_ty ->
                  let mono_target_ty = Aut.StateSet.fold (
                      fun q mono_target_ty ->
                        MonoAut.StateSet.add (PolyType.monomorphic q) mono_target_ty
                    ) target_ty MonoAut.StateSet.empty
                  in
                  if not (RegularTyper.RegularTypePartition.mem mono_target_ty ty_precision) then
                    raise (Error (Runtime (InvalidTargetType (mono_target_ty, ty_precision))));
                  Some mono_target_ty
                | None -> None
              in

              let type_system = TypeSystem.create mono_tc abstraction state_factory in
              let type_system = TypeSystem.declare_partition (type_of sub_typed_pattern) types type_system in
              let regular_typer = RegularTyper.create sub_typed_trs type_system FixpointLearner.find_fixpoint state_factory in

              (* Main call to regular language typer *)
              let typed_patterns, regular_typer = RegularTyper.type_pattern ty_precision sub_typed_pattern regular_typer in
                  if conf.abstraction_details then
                    begin
                      Format.printf "%t@." (format_abstraction (RegularTyper.values_abstraction regular_typer) (RegularTyper.abstraction regular_typer));
                      Format.printf "typed patterns:\n@.";
                    end;  
                  List.iter (
                    function typed_pattern ->
                      (* check if we found a counter example. *)
                     begin match mono_target_ty_opt with
                          | Some target_ty ->
                              let _, (ty, _) = typed_pattern in
                                  if not (RegularTyper.RegularType.equal ty target_ty) then
                                  raise (Error (Runtime (RegularTypeMissmatch (target_ty, typed_pattern, (RegularTyper.abstraction regular_typer)))))
                          | None -> ()
                    end;
                    (* show the typed pattern. *)
                    if conf.abstraction_details then
                      Format.printf "%t@." (RegularTyper.RegularTypedPattern.print typed_pattern)
                    else ()
                  ) typed_patterns
                end
        ) sub_typed_patterns;

        poly_typer
    ) patterns poly_typer);
  ()

(** Make a sequence of char out of an input channel. *)
let seq_of_channel input =
  let rec next mem () =
    match !mem with
    | Some res -> res
    | None ->
      let res =
        try
          let c = input_char input in
          Seq.Cons (c, next (ref None))
        with
        | End_of_file -> Seq.Nil
      in
      mem := Some res;
      res
  in
  next (ref None)

(* let build_spec lexer =
   try
    Build.specification lexer
   with exn -> Format.eprintf "error: %t" (format_error exn); exit 1 *)


(** General processing of timbuk specifications, where the [input_spec] is
    an utf8 sequence and [conf] contains the configuration information for
    the regular language typechecker.  *)
let process conf input_spec=
  begin try
      begin try
          let lexer = Lexer.create input_spec in
          let ast = Parse.specification lexer in
          Logs.debug (fun m -> m "parsed.");
          let spec = Build.specification ast in
          Logs.debug (fun m -> m "compiled.");
          process_spec conf spec;
          true
        with
        | Parse.Error (e, span) -> raise (Error (Parse (e, span)))
        | Build.Error (e, span) -> raise (Error (Build (e, span)))
        | PolyTyper.Error (e, span) -> raise (Error (PolyType (e, span)))
        | MonoTyper.Error (e, span) -> raise (Error (MonoType (e, span)))
        | SubTyper.Error (e, span) -> raise (Error (SubType (e, span)))
        | RegularTyper.Error e -> raise (Error (RegularType e))
        | FixpointLearner.IrreducibleNonValue term -> raise (Error (Runtime (IrreducibleNonValue term)))
        (* | Runtime.Error e -> raise (Error (Runtime (e, span))) *)
      end
    with
    (* Runtime error corresponding to a counter-example are left uncatched if we are in embedded mode *)
    | Error (Runtime (RegularTypeMissmatch (e1,e2,e3))) -> 
    (* [timbuk_embedded] is a boolean which 
    is true if timbuk is used inside another application (e.g. toplevel). In this 
    case, no exit should be performed when a counterexample is found.*)
      if conf.embedded 
      then 
        begin
        (* We print the counter-example but do not exit!*)
          format_error input_spec (Runtime (RegularTypeMissmatch (e1,e2,e3))) Format.err_formatter;
          false
        end
      else 
      begin
        format_error input_spec (Runtime (RegularTypeMissmatch (e1,e2,e3))) Format.err_formatter;
        exit 1
      end
    (* other errors are blocked and lead to an exit 1 *)
    | Error e ->
      format_error input_spec e Format.err_formatter;
      exit 1
  end

(** Command line interface main function: [filename] is the input file containting a
    timbuk specification. *)
let process_file conf filename =
  (* let open Log in *)
  Logs.info (fun m -> m "file: %s" filename);
  let file = open_in filename in
  let input = seq_of_channel file in
  let utf8_input = Unicode.Encoding.utf8_decode input in
    ignore (process conf utf8_input)

(**  External function call to main timbuk verification function
     using [input_string] which is a string containing a full timbuk specification.
     By default the configuration is Conf.default. *)
let process_string (input_string:string) =
  let conf= Conf.default in
    (* We set verbosity to 0 to avoid useless debug messages in the ocaml toplevel *)
  let conf= Conf.no_verbose conf in
  let conf= Conf.embedded conf in
  let conf= Conf.no_abstraction_details conf in
  (* install the log reporter *)
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level (Conf.log_level conf);
  (* let open Log in *)
  Logs.info (fun m -> m "file: %s" "user input");
  (* let patch_string = input_string ^"\n" in (* to overcome a bug in Timothée's parser *) *)
  let utf8_input = Utf8String.to_seq input_string in
    process conf utf8_input 

(* Main function call for command line interface *)
let _ =
  (* read options. *)
  let conf = Clap.parse spec Conf.default in

  (* install the log reporter. *)
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level (Conf.log_level conf);

  List.iter (process_file conf) conf.filenames
