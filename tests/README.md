# Testing

Each sub-directory contains tests using the `.timbuk` specification format.
This specification format is close to (but not exactly the same as) this one used by Timbuk 3.2.
It is detailed in the [Timbuk 4 Wiki pages](https://gitlab.inria.fr/regular-pv/timbuk/timbuk/-/wikis/home).
When possible, a `.timbuk3` file is provided, defining the same test for Timbuk 3.

This directory provides a Makefile to run all the tests, compare with other
tools (only Timbuk 3.2 for now) and generate comparison graphs.

## Dependencies

To run the tests suite, you will need to have the following installed on your computer:
  - Ruby (the test scripts are written in Ruby)
  - The GNU `time` command. It is most probably installed by default on GNU/Linux distributions, and can be installed on macOS using [Homebrew](https://brew.sh/): `brew install gnu-time` (it then should be called `gtime` but that's fine).
  - A `timeout` command.

Also you need to have compiled Timbuk 4 in the native format using either
`make` or `make native` in the root directory.

## Running the tests

You can run the tests using (**estimated runtime: 15 to 20 minutes** the first time):

	make test

For each `.timbuk` file, this will generate a `.log` file containing the output of the test,
and a `.bench` file containing statistics of the execution (time, memory usage, cpu usage).
You can use the following variant to run the tests with Timbuk 3.2 when possible
(**estimated runtime: 15 to 20 minutes** the first time):

	TIMBUK3=path/to/timbuk3 make test-timbuk3

Similarly, it will generate `.log3` and `.bench3` files.

You can safely run `make test` and `make test-timbuk3` multiple times without
overriding the previous benchmark files, it will append the result at the end.
Note that when a timeout is reached, the `timeout` output is added to the
benchmark file. *Future test execution will look into the previous benchmark
and skip tests that have already reached a timeout (or crashed or failed)*.
That way, the overall test time is reduced
(count 15 to 20 minutes for the first run, and 1 to 2 minutes for all the subsequent runs).
The following command can be used to run the tests 10 times so to average the execution stats (**estimated runtime: 25 to 40 minutes**):

	TIMBUK3=path/to/timbuk3 make test-full

It will append each execution stats to the end of each `.bench`/`.bench3` files.

Underneath this will run the `util/run` Ruby script dedicated to running tests.
Use `util/run --help` if you want to use this script directly and have more control
(over the timeout, number of threads used, etc.).
The basic usage typically looks like:

	util/run --count 1 --threads 2 -4 ../bin/timbuk
	util/run --count 1 --threads 2 -3 path/to/timbuk3

Behind the scene, for each specification file this script runs the following command (but with different parameters depending on the passed options):

	timeout 120 /usr/bin/time -f \"%U, %S, %e, %M, %P\" -ao file.bench timbuk file.timbuk > file.log

## Plotting

Once all the `.bench` and `.bench3` files have been generated, you can use the following command to plot graphs comparing this implementation with other tools (Timbuk 3.2 for now):

	make

You will need `gnuplot` installed on your system to run this command properly.
It will generate three `.eps` files:

  - `time.eps` comparing the execution time;
  - `memory.eps` comparing the memory usage;
  - `cpu.eps` comparing the CPU usage.

The more test executions the better:
the plotting scripts will average out all the execution stats stored into the benchmark files.

## Cleaning

Use `make clean` to remove the `.csv` files that have been generated on the way. Use `make mrproper` to also remove the `.eps` files.
Use `make test-clean` to remove the `.log` files,
or `make test-mrproper` to also remove the benchmark output files.

You can also use `make immaculate` to do all of the above at once.
