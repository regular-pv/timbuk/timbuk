#!/usr/bin/env ruby
require 'optparse'
require_relative 'lib.rb'

module CliOptionParser
	def self.parse(args)
		parser = OptionParser.new do |opts|
			opts.banner = "USAGE:\n    #{$0} [OPTIONS] FILE* columns COLUMN*"

			opts.separator ""
			opts.separator "OPTIONS:"

			opts.on("-h", "--help", "Print help information") do
				puts opts
				exit
			end

			opts.on("-g", "--tag NAME", "Tag filter") do |t|
				$tags << t
			end

			opts.on("-s", "--sort INDEX", "Column to use to sort the data") do |i|
				$sort_index << i
			end

			opts.separator "ARGS:"
			opts.separator "    FILE    CSV files to merge/filter"
			opts.separator "    COLUMN  Columns to put in the resulting CSV file"
		end

		parser.parse!(args)
	end
end

$root = ENV['TIMBUK4']
if $root.nil? then
	$root = ''
else
	$root += '/' unless $root[-1] == '/'
	$root += 'tests/'
end

$tags = []
$sort_index = 1

CliOptionParser.parse(ARGV)

$sep = ARGV.index('columns')
$sep = ARGV.index('cols') if $sep.nil?
$sep = ARGV.index('c') if $sep.nil?
raise 'missing "columns" separator' if $sep.nil?

$files = ARGV[0...$sep]
# $files = `#{$root}util/search`.split("\n") if $files.empty?
$columns = ARGV[($sep+1)..-1].map do |s|
	i = s.to_i
	raise 'not a number' if i.nil?
	i
end
$filter = true

$data = {}

def set(i, id, data)
	if i > 0 then
		if $filter then
			if $data[id].nil? || $data[id].size != i * $columns.size then
				$data.delete(id)
				return
			end
		else
			$data[id] = [] if $data[id].nil?
			$data[id] << '-' while $data[id].size != i * $columns.size
		end

		$data[id] += data
	else
		$data[id] = data
	end
end

def load(filename, i)
	File.open(filename, 'r') do |file|
		file.readlines.each do |line|
			line = line.split(',')
			path = line[0].strip
			entry = $root + path + '.timbuk'

			description, tags = read_metadata(entry)
			next unless $tags.all? { |tag|
				if tag[0] == '!' then
					!tags.include?(tag[1..-1])
				else
					tags.include?(tag)
				end
			}

			id = File.basename(path)
			data = []

			$columns.each do |i|
				data << line[i].strip
			end

			set(i, id, data)
		end
	end
end

$files.each.with_index do |filename, i|
	load(filename, i)
end

$data.select! { |id, data| data.size == $files.size * $columns.size }

$table = []
$data.each do |id, data|
	$table << [id] + data unless data.all? { |d| d.strip == '-' }
end

$table.sort_by! do |d|
	if $sort_index > 0 then
		d[$sort_index].to_f
	else
		d[$sort_index]
	end
end

$table.reverse!

$table.each do |d|
	puts d.join(', ')
end
