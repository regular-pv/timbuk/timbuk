set terminal eps enhanced font "arial,10" # size 2.65,2.75

# The grid
set grid ytics
set grid back lc 'black'

# Histogram style
set boxwidth 0.5 relative
set style fill solid
set style data histogram

# set yrange [0:45]
set format y '%.1s%cB'
set ylabel 'Memory usage'
set logscale y 10

# Rotate the x labels
set xtics rotate out

# Input file contains comma-separated values fields
set datafile separator ","

# Missing data indicator
set datafile missing '-'

# Setup the palette
set palette model RGB defined ( 0 '#aaaaaa', 1 '#666666', 2 '#000000' )
unset colorbox

set tmargin at screen 0.90; set bmargin at screen 0.45
plot filename using 2:xticlabels(1) title 'Ours', '' u 5 title 'Timbuk 3.2'
