class Entry
	attr_accessor :id
	attr_accessor :description
	attr_accessor :tags
	attr_accessor :status
	attr_accessor :time

	def initialize(id, description, tags, status, time)
		@id = id
		@description = description
		@tags = tags
		@status = status
		@time = time
	end
end

def read_metadata(filename)
	description = ""
	tags = []
	File.readlines(filename).each do |line|
		return description, tags unless line[0] == '%'

		if line.include?('@tags:') then
			tags = line.strip.split(/[,\ ]/)[2..-1].reject{ |e| e.empty? }
		else
			line = line[1..-1].strip
			unless line.empty?
				description += "\n" unless description.empty?
				description << line
			end
		end
	end

	return description, tags
end

def parse_time(str, timeout)
	return :timeout if str.to_f == timeout
	str.to_f
end

def read_csv(filename, timeout)
	items = []

	File.readlines(filename).each do |line|
		line = line.strip
		unless line.empty? then
			data = line.split(',')
			id = data[0].strip
			filename = id + '.timbuk'
			description, tags = read_metadata(filename)
			time = parse_time(data[1].strip, timeout)
			status = :timeout
			unless time == :timeout then
				if tags.include?('true') then
					status = :proved
				else
					status = :disproved
				end
			end
			status = :crashed if tags.include?('crash')
			items << Entry.new(id, description, tags, status, time)
		end
	end

	items
end
