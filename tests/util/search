#!/usr/bin/env ruby

$root = File.dirname(File.dirname(__FILE__))
$files = `find #{$root} -name '*.timbuk' -not -path '#{$root}/obselete/*'`.split("\n")
$tags = []
$cmd = :find
$editor = ENV['EDITOR']

ARGV.each do |arg|
	if arg == "open" && $tags.empty? && $cmd == :find then
		$cmd = :open
		if $editor.nil? then
			STDERR.puts 'please set the EDITOR environment variable'
			exit 1
		end
	else
		$tags << arg
	end
end

class SpecFile
	attr_accessor :filename
	attr_accessor :description
	attr_accessor :tags

	def initialize(filename, description, tags)
		@filename = filename
		@description = description
		@tags = tags
	end

	def inspect
		@filename
	end

	def tagged?(tags)
		tags.each do |tag|
			if tag[0] == '!' then
				return false unless !@tags.include?(tag[1..-1])
			else
				return false unless @tags.include?(tag)
			end
		end

		return true
	end
end

def read_spec_file(filename)
	description = ""
	tags = []
	File.readlines(filename).each do |line|
		return SpecFile.new(filename, description, tags) unless line[0] == '%'

		if line.include?('@tags:') then
			tags = line.strip.split(/[,\ ]/)[2..-1].reject{ |e| e.empty? }
		else
			line = line[1..-1].strip
			unless line.empty?
				description += "\n" unless description.empty?
				description << line
			end
		end
	end

	return SpecFile.new(filename, description, tags)
end

$specs = $files.map { |filename| read_spec_file(filename) }
$specs.select! { |spec| spec.tagged?($tags) }

case $cmd
when :find then
	$specs.each do |spec|
		puts spec.filename
	end
when :open then
	$specs.each.with_index do |spec, i|
		puts "[#{i+1}] #{spec.filename}"
	end
	print "which one(s) to open? [1-#{$specs.size}] "
	STDOUT.flush
	files = STDIN.gets.split.map do |str|
		i = str.strip.to_i
		unless !i.nil? && i > 0 && i <= $specs.size then
			STDERR.puts "invalid index `#{str}'"
			exit 1
		end

		$specs[i-1].filename
	end

	exec "#{$editor} #{files.join(' ')}"
end
