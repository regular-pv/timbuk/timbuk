
# Les bugs restants (?)

## non ascii chars

- quand il y a des accents dans les spécs (commentaires)
Fatal error: exception Failure("not implemented for non-ASCII chars.")

## nothing added
- tests/first-order/true/mergeSort2.timbuk    Fatal error: exception Failure("nothing added")
- tests/higher-order/true/insertionSortHO.timbuk 
Fatal error: exception Failure("nothing added") 

## cannot find fixpoint...
- tests/higher-order/true/foldDiv.timbuk 
Fatal error: exception Invalid_argument("cannot find fixpoint classes for this function.")

- tests/higher-order/true/foldDiv.timbuk 
Fatal error: exception Invalid_argument("cannot find fixpoint classes for this function.")

- ./bin/timbuk tests/first-order/false/reverseBUGimplies.timbuk 
Fatal error: exception Invalid_argument("cannot find fixpoint classes for this function.")

- ./bin/timbuk tests/first-order/true/orderedTree.timbuk 
Fatal error: exception Invalid_argument("cannot find fixpoint classes for this function.")

## autres bugs (?)
- tests/first-order/true/plusImplies.timbuk
  Fatal error: exception Failure("typing/src/regular.ml: produced automaton is not a valid solution, but no counter example was found. This is a bug.")
  
- --no-refine-consts tests/mochi/foldFunListMochi.timbuk 
  mais bien heureux celui qui sait où est passé cet exemple! Timothée?
  
- --no-refine-consts tests/first-order/true/mergeSort2.timbuk
  done un contrex (après un moment)
  
prop('0:{#2}, '5:{#24}):{qtrue}
runtime error: expected regular type `{qtrue}', found `{#0}'
note: here is a term that may not rewrite to {qtrue}:

      prop(B, cons(A, cons(A, cons(B, nil))))


# Ce qui ne marche pas (diverge)

  - tests/higher-order/true/mapSquare.timbuk (timbuk 3 le passait)... doit être le même problème que pour le eq premier ordre <-> eq ordre sup

- tests/higher-order/true/exprSimplif.timbuk
  diverge???
  
- tests/higher-order/true/leq.timbuk 
  Rame... Quand on écrit "eq s(0) X", il arrive effectivement à extraire les
constantes "0", "s(0)", "eq s(0)". Mais le problème, c'est que pour
typer correctement le système de réécriture de sorte que l'analyse
termine comme au premier ordre, il faudrait aussi ajouter la constante
"eq 0". Or elle n'apparaît pas directement dans le TRS.
Elle apparaît indirectement par réécriture du terme "eq s(0) X". Mais
si je commence à suivre les règles de réécriture pour extraire les
constantes, on va vite perdre la terminaison de l'algo d'extraction... 


- redBlackPredicate.timbuk ...
	stoppé au bout de 2h.

- orderedTreeTraversal.timbuk .
    diverge?

- zipList.timbuk : diverge mais c'est normal... ne prend pas en compte l'hypothèse à gauche du implies

- test/first-order/false/deleteBUG2.txt (devrait s'arrêter aussi même raison qu'au dessus).

- insertTree (arbres) ... >2h... pas de réponse
    timbuk3 ne le passait pas non plus
