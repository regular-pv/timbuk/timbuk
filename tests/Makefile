BENCH3:=$(shell find . -name '*.bench3')
BENCH4:=$(shell find . -name '*.bench')
LOG3:=$(shell find . -name '*.log3')
LOG4:=$(shell find . -name '*.log')

all: time.eps memory.eps cpu.eps

# Plot

timbuk3.csv: $(BENCH3)
	./util/compile $^ > timbuk3.csv

timbuk4.csv: $(BENCH4)
	./util/compile $^ > timbuk4.csv

time.csv: timbuk4.csv timbuk3.csv
	util/merge $^ columns 1 2 3 > time.csv

time.eps: time.csv util/time.plot
	gnuplot -e "filename='$<'" util/time.plot > $@

memory.csv: timbuk4.csv timbuk3.csv
	util/merge $^ columns 4 5 6 > memory.csv

memory.eps: memory.csv util/memory.plot
	gnuplot -e "filename='$<'" util/memory.plot > $@

cpu.csv: timbuk4.csv timbuk3.csv
	util/merge $^ columns 7 8 9 > cpu.csv

cpu.eps: cpu.csv util/cpu.plot
	gnuplot -e "filename='$<'" util/cpu.plot > $@

clean:
	rm -f timbuk3.csv timbuk4.csv
	rm -f time.csv memory.csv cpu.csv
	rm -f comp.res comp.res.epsi

mrproper: clean
	rm -f time.eps memory.eps cpu.eps

# Running tests

test-all: test test3

test:
	./util/run --count 1 --threads 1 -4 ../bin/timbuk

test-timbuk3:
	./util/run --count 1 --threads 1 -3 $(TIMBUK3)

test-full: test-all
	./util/run --count 9 --threads 2 --fast -4 ../bin/timbuk
	./util/run --count 9 --threads 2 --fast -3 $(TIMBUK3)

test-clean:
	rm -f $(LOG3) $(LOG4)

test-mrproper: test-clean
	rm -f $(BENCH3) $(BENCH4)

# Misc

immaculate: mrproper test-mrproper

.PHONY: all clean mrproper tests-all test test3 test-full test-clean test-mrproper immaculate
