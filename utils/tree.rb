#!/usr/bin/env ruby

PREFIX = "timbuk.byte: [DEBUG] "
PREFIX_LEN = PREFIX.size

def display(tabs, line)
	puts ("\t" * tabs) + line
end

tabs = 0
STDIN.read.split("\n").each do |line|
	if line[0...PREFIX_LEN] == PREFIX then
		line = line[PREFIX_LEN..-1]
		case line[0]
		when "+" then
			display(tabs, line)
			tabs += 1
		when "-" then
			tabs -= 1
			display(tabs, line)
		when ":" then
			display(tabs, line)
		else
			# display(tabs, line)
		end
	end
end
