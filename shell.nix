with import <nixpkgs> {};

runCommand "dummy" {
		buildInputs = [
		dune
		ocamlPackages_latest.ocaml
		ocamlPackages_latest.findlib
		ocamlPackages_latest.logs
		ocamlPackages_latest.fmt
		ocamlPackages_latest.odoc
		binutils-unwrapped
		gcc

		cvc4
	];

	shellHook = ''
		export OCAMLPATH="`pwd`/.local/lib:$OCAMLPATH"
		export PREFIX="`pwd`/.local"
		export PATH="`pwd`/.local/bin:$PATH"
  '';
} ""
