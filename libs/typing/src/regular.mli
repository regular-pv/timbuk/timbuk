open Collections
open Timbuk
open TimbukSolving

module type TYPED_PATTERN = TypedPattern.S
module type SOLVER = Solver.S

(** Internal representation of partitions that may be unknown. *)
type 'partition abs_partition =
  (** The partition is known. *)
  | Known of 'partition
  (** The partition is unknown and sould be inferred. *)
  | Unknown

(** An abstract type. Couple of a type partition and its domain. *)
type ('q, 'partition) abs_type = ('partition abs_partition) * 'q

(** States internally generated by the analysis. *)
type ('q, 'partition) solved_state =
  (** Old states that are part of the previous abstraction. *)
  | Old of 'q
  (** New states that have been added to the abstraction. *)
  | New of 'partition abs_partition * 'q * int

(** Regular type inference module signature. *)
module type S = sig
  (** Type used as pattern location.
      The patterns to type are usually comming from some source file (for
      instance, a Timbuk specification file).
      The [Location.t] type is used to map each (sub)pattern to its location in
      the file.
      This is useful to locate the source of eventual typing errors. *)
  module Location : sig type t end

  (** Type system. *)
  module TypeSystem : RefinedTypeSystem.S

  (** Type automaton (used in the type system). *)
  module TypeAut = TypeSystem.TypeAut

  (** Typed pattern. Input of the procedure.
      Note that each type is accompagned with the location of the pattern. *)
  module TypedPattern : TypedPattern.S with type Sym.t = TypeAut.Sym.t and type Type.t = TypeAut.State.t * Location.t

  (** Typed term rewriting system. Input of the procedure. *)
  module TypedTrs : Relation.S with type ord = TypedPattern.t and type elt = TypedPattern.t * TypedPattern.t

  (** Regular language type (a set of states in the type automaton). *)
  module RegularType : TypedTerm.TYPE with type t = TypeAut.StateSet.t

  (** Regular type partition.
      Represents a set of disjoined regular types. *)
  module RegularTypePartition : Set.S with type elt = RegularType.t

  (** Regular typed pattern. *)
  module RegularTypedPattern : TYPED_PATTERN with type Sym.t = TypeAut.Sym.t and type Var.t = TypedPattern.Var.t and type Type.t = RegularType.t * Location.t

  (** Regular type inference errors. *)
  type error =
    (** Some pattern can be typed with two member of a regular type partition. *)
    | BiTypedTerm of (TypeAut.Sym.t Term.term * TypeAut.State.t * TypeAut.State.t * RegularTypePartition.t)

  (** Exception raised by the inference procedure. *)
  exception Error of error

  (** Inference instance.  *)
  type t

  (** The abstraction of values. *)
  val values_abstraction : t -> TypeAut.t

  (** The whole abstraction, including values and functions abstractions. *)
  val abstraction : t -> TypeAut.t

  (** [type_pattern target_partition pattern t] infers all the regular type
      decorations possible for the given input [pattern] and target type partition
      [target_partition]. *)
  val type_pattern : RegularTypePartition.t -> ?forbid:TypeAut.StateSet.t -> TypedPattern.t -> t -> RegularTypedPattern.t list * t
end

(** Create a new regular type inference module. *)
module Make
    (Location : sig type t end)
    (TypeSystem : RefinedTypeSystem.S)
    (TypedPattern : TypedPattern.S with type Sym.t = TypeSystem.TypeAut.Sym.t and type Type.t = TypeSystem.TypeAut.State.t * Location.t)
    (TypedTrs : Relation.S with type ord = TypedPattern.t and type elt = TypedPattern.t * TypedPattern.t)
    (Solver : SOLVER with type Var.t = TypeSystem.TypeAut.State.t)
    (MinimalSolver : SOLVER with type Var.t = int) :
sig
  include S with module Location = Location
             and module TypeSystem = TypeSystem
             and module TypedPattern = TypedPattern
             and module TypedTrs = TypedTrs

  module AbsType : Automaton.STATE with type t = (TypeSystem.TypeAut.State.t, RegularTypePartition.t) abs_type
  module AbsTypedPattern : TYPED_PATTERN with type Sym.t = TypeAut.Sym.t and type Var.t = TypedPattern.Var.t and type Type.t = AbsType.t * Location.t
  module AbsTypedTrs : Relation.S with type ord = AbsTypedPattern.t

  module SolvedAut : Automaton.S with type Sym.t = TypeAut.Sym.t and type State.t = (TypeAut.State.t, RegularTypePartition.t) solved_state and type Label.t = unit and type data = unit

  (** Fixpoint learning function.
      For a given type system and TRS (abstractly typed), gives an abstraction for the TRS.
      See the Timbuk Fixpoint Learning module for more informations. *)
  type fixpoint_learner = TypeSystem.t -> AbsTypedTrs.t -> SolvedAut.t option

(** [create trs type_system fixpoint_learner state_factory] creates a new regular type inference instance where
    [trs] is the program, [type_system] is holds the initial abstraction/type system,
    [fixpoint_learner] gives the invariant learning procedure and
    [state_factory] gives generate new states for the type automaton
    (the generated state/type will be a sub-type of the input state/type). *)
  val create : TypedTrs.t -> TypeSystem.t -> fixpoint_learner -> (TypeAut.State.t -> TypeAut.State.t) -> t
end
