# Timbuk 4 - Term sampler

The purpose of this library is to provide means of exploring a language in a
fair manner. It is for instance used in the regular typing algorithm to
generate learning samples.
