(* open Timbuk

module Make (F: Symbol.S) (Q: Automaton.STATE) (N: Automaton.STATE) : sig
  include Sampler.S with module Sym = F
                     and module State = Q
                     and module Node = N

  val create : (State.t -> Node.t) -> t
end *)
