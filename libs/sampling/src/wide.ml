(* open Timbuk

module Make (F: Symbol.S) (Q: Automaton.STATE) (N: Automaton.STATE) = struct
  module Sym = F
  module State = Q
  module Node = N

  module Seed = Pattern.Make (F) (Q)

  type transition = (Sym.t * Node.t list)

  type t = ()

  let add seed t =
    t

  let fold_new_sub_nodes t f l x =
    ExtList.fold_bool_combinations (
      fun flag_list x ->
        if List.exists (function x -> x) flag_list then
          begin
            let l'_domains = List.map2 (
                function q' -> function
                  | true -> Aut.StateMap.find q' t.new_nodes
                  | false -> Aut.StateMap.find q' t.nodes
              ) l flag_list
            in
            NodeSet.fold_inline_combinations f l'_domains x
          end
        else
          x
    ) (List.length l) x

  let grow_state_seed t q new_transitions =
    let q_nodes = Aut.StateMap.find q t.nodes in
    let q_new_nodes = Aut.StateMap.find q t.new_nodes in
    let confs = Aut.configurations_for_state q t.type_knowledge in
    let t, new_new_nodes = Aut.LabeledConfigurationSet.fold (
        fun (conf, _) (t, new_nodes, new_transitions) ->
          match conf with
          | Aut.Configuration.Cons (f, l) ->
            (* debug "process configuration: %t@." (Aut.Configuration.print conf); *)
            let l = List.map Aut.Configuration.normalized l in
            fold_new_sub_nodes t (
              fun l' (t, new_nodes) ->
                let abs_conf = AbsAut.Configuration.Cons (f, List.map AbsAut.Configuration.of_var l') in
                let new_node = Node.create_from_abstract q in
                let new_nodes = NodeSet.add new_node new_nodes in
                let t = add_transition t abs_conf new_node in
                t, new_nodes
            ) l (t, new_nodes)
          | _ ->
            failwith "impossible"
      ) confs (t, NodeSet.empty, new_transitions)
    in
    {
      t with
      nodes = Aut.StateMap.add q (NodeSet.union q_nodes q_new_nodes) t.nodes;
      new_nodes = Aut.StateMap.add q new_new_nodes t.new_nodes
    }

  let grow_seed (t : t) ((f, l, ty) : Seed.t) =
    (* debug "grow seed@."; *)
    fold_new_sub_nodes t (
      fun l' t ->
        (* debug "grow seed conf@."; *)
        let conf = AbsAut.Configuration.Cons (f, List.map AbsAut.Configuration.of_var l') in
        let new_node, t = add_conf t conf ty in
        {
          t with
          new_seed_nodes = NodeSet.add new_node t.new_seed_nodes
        }
    ) l t

  let grow t =
    (** Here we grow every state. *)
    let t = StateMap.fold (
        fun q _ t ->
          grow_state_seed t q
      ) t.nodes t
    in

    (** Now we can grow the actual seeds. *)
    let t = SeedSet.fold (
        fun seed t ->
          grow_seed t seed
      ) t.seeds t
    in
    t
end *)
