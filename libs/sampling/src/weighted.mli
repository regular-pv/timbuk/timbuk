open Timbuk

(** Weighted sampler, able to generate fair runs and privilegiating
    the shortest terms. The goal is to avoid the exponential blow-up of the
    `Wide` sampler. *)

(** Functor used to instanciate the Weighted sampler. *)
module Make (F: Symbol.S) (Q: Automaton.STATE) (L: Automaton.LABEL) (T: Automaton.S with type Sym.t = F.t and type Label.t = unit) : sig
  type type_system = {
    automaton: T.t;
    (** The type automaton. An automaton in which each state is a type,
        recognizing every value if this type. *)

    state_type: Q.t -> T.State.t;
    (** Map a state to its type. *)

    normalizer: T.State.t -> (Q.t * L.t)
    (** State generation. *)
  }

  include Sampler.S with module Sym = F
                     and module State = Q
                     and module Label = L
                     and module TypeAut = T
                     and type data = type_system
end
