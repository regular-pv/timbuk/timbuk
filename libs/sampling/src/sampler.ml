open Timbuk

module type S = sig
  include Automaton.S

  module TypeAut : Automaton.S with type Sym.t = Sym.t and type Label.t = unit

  type seed = (Sym.t * TypeAut.State.t list * TypeAut.State.t)

  val handle : seed -> t -> t * (Configuration.t * Label.t * State.t) list

  val grow : t -> t * (Configuration.t * Label.t * State.t) list

  val size : t -> int
end
