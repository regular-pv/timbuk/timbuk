open Timbuk

(** A term sampler represent an automaton recognizing a family of terms
    built from given seeds. At first, the automaton is empty, but each call
    to the `grow` function will add new terms to the automaton matching the
    seed.
    The sampler is not actually building the automaton, but instead provide
    a list of new transitions to add to the automaton as the output of a call
    to grow.
    A sampler implementation is expected to be `fair`, in the sense that each
    term of the target language will eventually be added to the automaton,
    for enough call to the `grow` function.
    The given State module specify the type of each state of the automaton. Since
    the sampler will create many states, `Q` must provide a `spawn` method to
    create fresh states. *)
module type S = sig
  include Automaton.S

  module TypeAut : Automaton.S with type Sym.t = Sym.t and type Label.t = unit

  type seed = (Sym.t * TypeAut.State.t list * TypeAut.State.t)

  (** Add a new seed to the sampler. The generated terms a guaranteed to be
      members of the given seeds. Terms are built in a fair manner. *)
  val handle : seed -> t -> t * (Configuration.t * Label.t * State.t) list

  (** Add new terms to the automaton.
      The new terms are part of the language defined by the seeds,
      however there are no guaranties about the complexity of this function. *)
  val grow : t -> t * (Configuration.t * Label.t * State.t) list

  val size : t -> int
end
