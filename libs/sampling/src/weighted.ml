open Collections
open Timbuk

let log_src = Logs.Src.create "timbuk.sampling.weighted"
module Log = (val Logs.src_log log_src : Logs.LOG)

module Make (F: Symbol.S) (Q: Automaton.STATE) (L: Automaton.LABEL) (T: Automaton.S with type Sym.t = F.t and type Label.t = unit) = struct
  module TypeAut = T

  module Type = TypeAut.State

  module SignedSymbol = struct
    type t = (F.t * Type.t list * Type.t)

    let hash (f, l, t) =
      List.fold_left (fun h ty -> h lxor (Type.hash ty)) ((F.hash f) lxor (Type.hash t)) l

    let compare (fa, la, ta) (fb, lb, tb) =
      let c = F.compare fa fb in
      if c = 0 then
        let c = Type.compare ta tb in
        if c = 0 then
          List.compare Type.compare la lb
        else c
      else c

    let equal (fa, la, ta) (fb, lb, tb) =
      F.equal fa fb && Type.equal ta tb && List.equal Type.equal la lb

    let print (f, l) out =
       Format.fprintf out "%t(%t)" (F.print f) (List.print Type.print ", " l)
  end

  (* module SignedSymbol = Signed (Type) *)

  (* module Seed = Signed (Q) *)

  module SignedSymbolTable = HashTable.Make (SignedSymbol)

  module TypeTable = HashTable.Make (Type)

  (* module SeedTable = HashTable.Make (Seed) *)

  module WeightedState = struct
    (** The state and the number of times it occurs inside a configuration of the considered family. *)
    type t = (Q.t * int)

    let matches q (q', _) = q = q'
  end

  module WeightedVec = struct
    type t = (WeightedState.t Vec.t * int)

    let create () =
      (Vec.create 8, 0)

    let of_value_list values =
      let ary = Vec.of_list (List.map (function q -> (q, 0)) values) in
      (ary, 0)

    let length (vec, _) =
      Vec.length vec

    let rec reorder_up i vec =
      if i = 0 then vec else
        let j = i - 1 in
        let i_occurences = Vec.get i vec in
        let j_occurences = Vec.get j vec in
        if j_occurences < i_occurences then
          reorder_up j (Vec.swap i j vec)
        else
          vec

    let incr q (vec, count) =
      try
        let ((_, occurences), index) =  Vec.find_last (WeightedState.matches q) vec in
        let vec = reorder_up index (Vec.set index (q, occurences + 1) vec) in
        (vec, count + 1)
      with
      | Not_found -> (vec, count)

    let choose n_combinations (vec, count) =
      begin match Vec.length vec with
        | 0 -> None
        | len ->
          let max_occurences = n_combinations / len in
          let proba (_, occurences) = (float_of_int (max_occurences - occurences)) /. (float_of_int (n_combinations - count)) in

          let threshold = Random.float 1.0 in
          let acc = ref 0.0 in
          let choosen q =
            let p = proba q in
            acc := !acc +. p;
            !acc >= threshold
          in

          begin match Vec.find_last_opt choosen vec with
            | Some ((q, _), _) -> Some q
            | None -> None
          end
      end

    let add q (vec, count) =
      (* Log.print "add %t\n" (Q.print q); *)
      let vec' = Vec.push (q, 0) vec in
      (vec', count)

    let append qs t =
      List.fold_right add qs t
  end

  type type_system = {
    automaton: TypeAut.t;

    state_type: Q.t -> Type.t;

    normalizer: Type.t -> (Q.t * L.t)
  }

  module Base = struct
    module Sym = F
    module State = Q
    module Label = L

    module Configuration = Pattern.Make (Sym) (State)

    module LabeledConfiguration = struct
      type t = Configuration.t * Label.t

      let compare (a, la) (b, lb) =
        let c = Label.compare la lb in
        if c = 0 then Configuration.compare a b else c

      let print (c, l) out =
        Configuration.print c out;
        Label.print l out
    end

    module LabeledState = struct
      type t = State.t * Label.t

      let product (a, la) (b, lb) =
        match State.product a b, Label.product la lb with
        | Some c, Some lc -> Some (c, lc)
        | _, _ -> None

      let compare (a, _) (b, _) = State.compare a b

      let equal (a, _) (b, _) = State.equal a b

      let hash (q, _) = State.hash q

      let print (q, l) out =
        Label.print l out;
        State.print q out
    end

    module StateSet = Set.Make (State)
    module StateMap = Map.Make (State)

    module ConfigurationSet = Set.Make (Configuration)
    module ConfigurationMap = Map.Make (Configuration)

    module LabeledStateSet = Set.Make (LabeledState)
    module LabeledConfigurationSet = Set.Make (LabeledConfiguration)

    type transition = Configuration.t * Label.t * State.t

    type t = {
      ts: type_system;
      (** Type system informations. *)

      table: (WeightedVec.t list) SignedSymbolTable.t;
      (** Associate each signed symbol to its weighted node arrays.
          Note that we don't actually store constant symbols here. *)

      seed_table: (WeightedVec.t list * Type.t) SignedSymbolTable.t;
      (** Associate each seed to its weighted node arrays.
          Note that we don't actually store constant seeds here. *)

      values: (State.t list) TypeTable.t;
      (** The values created for each type. *)

      new_values: (State.t list) TypeTable.t;
      (** New values created during this cycle. *)

      new_transitions: transition list;
      (** New transitions created during this cycle. *)

      roots : StateSet.t; (* Final states. *)
      state_confs : LabeledConfigurationSet.t StateMap.t; (* Associates to each state the set of configurations leading to it. *)
      conf_states : LabeledStateSet.t ConfigurationMap.t; (* Associates to each configuration the set of states to go to. *)
      state_parents : ConfigurationSet.t StateMap.t; (* Associates to each state the set of configurations it appears in. *)
    }

    type data = type_system

    let create type_system = {
      ts = type_system;
      table = SignedSymbolTable.create 8;
      seed_table = SignedSymbolTable.create 8;
      values = TypeTable.create 8;
      new_values = TypeTable.create 8;
      new_transitions = [];
      roots = StateSet.empty;
      state_confs = StateMap.empty;
      conf_states = ConfigurationMap.empty;
      state_parents = StateMap.empty
    }

    let data t =
      t.ts

    let final_states a = a.roots

    let configurations_for_states a =
      a.state_confs

    let states_for_configurations a =
      a.conf_states

    let state_parents q a =
      match StateMap.find_opt q a.state_parents with
      | Some set -> set
      | None -> ConfigurationSet.empty

    let add_final_state q a = {
      a with
      roots = StateSet.add q (a.roots)
    }

    let add_final_states set a = {
      a with
      roots = StateSet.union set (a.roots)
    }

    let set_final_states set a = {
      a with
      roots = set
    }

    type hook = Configuration.t -> Label.t -> State.t -> unit

    let configurations_for_state q a =
      match StateMap.find_opt q a.state_confs with
      | Some set -> set
      | None -> LabeledConfigurationSet.empty

    let states_for_configuration conf a =
      match ConfigurationMap.find_opt conf a.conf_states with
      | Some set -> set
      | None -> LabeledStateSet.empty

    let mem_transition conf label q t =
      let confs = configurations_for_state q t in
      LabeledConfigurationSet.mem (conf, label) confs

    let register_configuration f l _ q t =
      let ty = t.ts.state_type q in
      let signed_f = (f, List.map t.ts.state_type l, ty) in
      (* Log.print "register %t\n" (SignedSymbol.print signed_f); *)
      begin match l with
        | [] ->
          let conf = TypeAut.Configuration.Cons (f, []) in
          if TypeAut.LabeledStateSet.is_empty (TypeAut.states_for_configuration conf t.ts.automaton) then
            t
          else
            let new_values = match TypeTable.find_opt ty t.new_values with
              | Some new_values -> q::new_values
              | None -> q::[]
            in
            {
              t with
              new_values = TypeTable.set ty new_values t.new_values
            }
        | _ ->
          begin match SignedSymbolTable.find_opt signed_f t.table with
            | Some vecs ->
              let new_vecs = List.map2 WeightedVec.incr l vecs in
              let new_values = match TypeTable.find_opt ty t.new_values with
                | Some new_values -> q::new_values
                | None -> q::[]
              in
              {
                t with
                table = SignedSymbolTable.set signed_f new_vecs t.table;
                new_values = TypeTable.set ty new_values t.new_values
              }
            | None ->
              begin match SignedSymbolTable.find_opt signed_f t.seed_table with
                | Some (vecs, ty) ->
                  let new_vecs = List.map2 WeightedVec.incr l vecs in
                  {
                    t with
                    seed_table = SignedSymbolTable.set signed_f (new_vecs, ty) t.seed_table
                  }
                | None -> t
              end
          end
      end

    let clear t =
      {
        t with
        roots = StateSet.empty;
        state_confs = StateMap.empty;
        conf_states = ConfigurationMap.empty;
        state_parents = StateMap.empty
      }

    let add_transition_unchecked conf label q t =
      let add_parent_to q parents = StateMap.add q (ConfigurationSet.add conf (state_parents q t)) parents in
      {
        t with
        state_confs = StateMap.add q (LabeledConfigurationSet.add (conf, label) (configurations_for_state q t)) t.state_confs;
        conf_states = ConfigurationMap.add conf (LabeledStateSet.add (q, label) (states_for_configuration conf t)) t.conf_states;
        state_parents = Configuration.fold add_parent_to conf t.state_parents;
      }

    let add_transition ?hook conf label q t =
      (* Log.debug "TRY ADD CONF %t -> %t\n" (Configuration.print conf) (State.print q); *)
      if mem_transition conf label q t then begin
        t
      end else begin
        let t = begin match conf with
          | Configuration.Cons (f, l) ->
            register_configuration f (List.map Configuration.normalized l) label q t
          | _ -> t
        end in
        begin match hook with
          | Some h -> h conf label q
          | None -> ()
        end;
        let t = add_transition_unchecked conf label q t in
        { t with new_transitions = (conf, label, q)::t.new_transitions }
      end

    let merge _ _ =
      failwith "TODO: TimbukSampler.Weighted.merge not defined"
  end

  include Automaton.Extend (Base)

  type seed = SignedSymbol.t

  let create_wstate_vec ty t =
    begin match TypeTable.find_opt ty t.Base.values with
      | Some values -> WeightedVec.of_value_list values
      | None -> WeightedVec.create ()
    end

  let normalizer t ty f l =
    (* let l_types = List.map t.Base.ts.state_type l in *)
    (* let signed_f = (f, l_types, ty) in *)
    (* let conf = TypeAut.Configuration.Cons (f, List.map TypeAut.Configuration.of_var l_types) in *)
    (* let types = TypeAut.states_for_configuration conf t.Base.ts.automaton in *)
    (* let ty = match TypeAut.LabeledStateSet.cardinal types with
      | 1 -> fst (TypeAut.LabeledStateSet.choose types)
      | 0 ->
        begin match SignedSymbolTable.find_opt signed_f t.Base.seed_table with
          | Some (_, ty) -> ty
          | None ->
            (* Log.error "unable to type: %t\n" (SignedSymbol.print signed_f); *)
            raise (Invalid_argument "unable to type configuration")
        end
      | _ -> raise (Invalid_argument "ambiguity")
    in *)
    t.Base.ts.normalizer ty

  let add_normalized_configuration (f, l, ty) t =
    let conf = TypeAut.Configuration.Cons (f, List.map Configuration.of_var l) in
    let states = states_for_configuration conf t in
    if LabeledStateSet.exists (function (q, _) -> Type.equal (t.Base.ts.state_type q) ty) states then t
    else begin
      let q, label = t.Base.ts.normalizer ty in
      add_transition conf label q t
    end

  (** Start generating values for the given configuration of the given type. *)
  let rec handle_configuration ty conf t =
    (* Log.debug "HANDLE: %t:%t@." (TypeAut.Configuration.print conf) (Type.print ty); *)
    begin match conf with
      | TypeAut.Configuration.Var _ -> failwith "non normalized configuration"
      | TypeAut.Configuration.Cons (f, []) ->
        (* let conf = Configuration.Cons (f, []) in *)
        (* let _, t = add_configuration (normalizer t ty) conf t in *)
        add_normalized_configuration (f, [], ty) t
      | TypeAut.Configuration.Cons (f, l) ->
        let normalized_l = List.map TypeAut.Configuration.normalized l in
        let signed_f = (f, normalized_l, ty) in
        begin match SignedSymbolTable.find_opt signed_f t.Base.table with
          | Some _ -> t (* configuration is already handled. *)
          | None ->
            let vecs = List.map (function ty -> create_wstate_vec ty t) normalized_l in
            let t = {
              t with
              table = SignedSymbolTable.set signed_f vecs t.table
            }
            in
            List.fold_right handle_type normalized_l t
        end
    end

  and handle_labeled_configuration ty (conf, _) t =
    handle_configuration ty conf t

  (** Start generating values for the given type. *)
  and handle_type ty t =
    let configurations = TypeAut.configurations_for_state ty t.Base.ts.automaton in
    TypeAut.LabeledConfigurationSet.fold (handle_labeled_configuration ty) configurations t

  (* let fold_new_sub_states t f l x =
     List.fold_bool_combinations (
      fun flag_list x ->
        if List.exists (function x -> x) flag_list then
          begin
            let l'_domains = List.map2 (
                function ty -> function
                  | true -> TypeTable.find ty t.new_values
                  | false -> TypeTable.find ty t.values
              ) l flag_list
            in
            NodeSet.fold_inline_combinations f l'_domains x
          end
        else
          x
     ) (List.length l) x *)

  let commit_configuration t (_, l, _) vecs =
    (* Log.print "commit to configuration %t\n" (SignedSymbol.print (f, l)); *)
    let new_values_for ty =
      match TypeTable.find_opt ty t.Base.new_values with
      | Some new_values -> new_values
      | None ->
        (* Log.print "no new values...\n"; *)
        []
    in
    List.map2 WeightedVec.append (List.map new_values_for l) vecs

  let commit_seed_configuration t signed_f (vecs, ty) =
    (commit_configuration t signed_f vecs, ty)

  let commit t =
    let new_table = SignedSymbolTable.map (commit_configuration t) t.Base.table in
    let new_seed_table = SignedSymbolTable.map (commit_seed_configuration t) t.Base.seed_table in
    if SignedSymbolTable.size new_seed_table <> SignedSymbolTable.size t.Base.seed_table then failwith "???";
    {
      t with
      Base.table = new_table;
      seed_table = new_seed_table;
      values = TypeTable.union (fun _ a b -> Some (a @ b)) t.Base.values t.Base.new_values;
      new_values = TypeTable.create 8;
      new_transitions = []
    }

  let handle seed t =
    begin match seed with
      | (f, [], ty) ->
        (* let conf = Configuration.Cons (f, []) in *)
        (* let _, t = add_configuration (normalizer t ty) conf t in *)
        let t = add_normalized_configuration (f, [], ty) t in
        t, t.new_transitions
      | (f, l, ty) ->
        begin match SignedSymbolTable.find_opt seed t.Base.seed_table with
          | Some _ -> t, t.new_transitions (* seed is already handled. *)
          | None ->
            let vecs = List.map (function ty -> create_wstate_vec ty t) l in
            let t = {
              t with
              seed_table = SignedSymbolTable.set seed (vecs, ty) t.seed_table
            }
            in
            let t = List.fold_right handle_type l t in
            t, t.new_transitions
        end
    end

  let grow_configuration t (f, l, ty) (vecs : WeightedVec.t list) =
    (* Log.debug (fun m -> m "considering %t\n@." (SignedSymbol.print (f, l))); *)
    let n_combinations = List.fold_left (fun n vec -> n * (WeightedVec.length vec)) 1 vecs in
    begin match List.map_opt (WeightedVec.choose n_combinations) vecs with
      | Some l ->
        (* let conf = Configuration.Cons (f, List.map Configuration.of_var l) in *)
        (* let _, t = add_configuration (normalizer t ty) conf t in *)
        add_normalized_configuration (f, l, ty) t
      | None -> t
    end

  let grow_seed_configuration t signed_f (vecs, _) =
    grow_configuration t signed_f vecs

  let size t =
    (SignedSymbolTable.size t.Base.seed_table) + (SignedSymbolTable.size t.Base.table)

  let grow t =
    (* Log.print "GROWING...\n@."; *)
    (* Log.print "GROW, size %d in\n%t\n\nin%t\n@." (size t) (print t) (TypeAut.print t.Base.ts.automaton); *)
    let t = commit t in
    let t = SignedSymbolTable.fold grow_configuration t t.Base.table in
    let t = SignedSymbolTable.fold grow_seed_configuration t t.Base.seed_table in
    (* Log.print "GROWN!\n@."; *)
    (* Log.print "DONE, size %d in \n%t\n@." (size t) (print t); *)
    t, t.new_transitions
end
