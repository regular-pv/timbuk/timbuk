(** Timbuk Sampling

    Provides multiple language generators.
    Each module is able to generate and grow an automaton recognizing a finite
    subset of a given regular language, in a fair way.
*)

module Sampler = Sampler

module Weighted = Weighted

module Wide = Wide
