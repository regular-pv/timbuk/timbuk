open Timbuk
open TimbukUtils
open TimbukSampler

module TypeAutomaton = Automaton.Make (StringSymbol) (StringState) (NoLabel)

module Type = StringState

module MaybeRefinedType = struct
  type t =
    | Sample of Type.t * int

  let count = ref 0

  let next_id () =
    let id = !count in
    count := id + 1;
    id

  let next_sample ty =
    Sample (ty, next_id ())

  let equal a b =
    begin match a, b with
      | Sample (ta, na), Sample (tb, nb) -> na = nb && Type.equal ta tb
    end

  let compare a b =
    begin match a, b with
      | Sample (ta, na), Sample (tb, nb) ->
        let c = compare na nb in
        if c = 0 then Type.compare ta tb else c
    end

  let hash = Hashtbl.hash

  let product _ _ = failwith "no product defined"

  let print t out =
    begin match t with
      | Sample (ty, n) -> Format.fprintf out "%t#%d" (Type.print ty) n
    end
end

module Sampler = Weighted.Make (StringSymbol) (MaybeRefinedType) (NoLabel) (TypeAutomaton)


let tt = StringSymbol.create "true" 0
let ff = StringSymbol.create "false" 0
let a = StringSymbol.create "a" 0
let b = StringSymbol.create "b" 0
let nil = StringSymbol.create "nil" 0
let cons = StringSymbol.create "cons" 2
let sorted = StringSymbol.create "sorted" 1

let bool_t = StringState.of_string "bool"
let ab_t = StringState.of_string "ab"
let list_t = StringState.of_string "list"

let ty_automaton =
  let aut = TypeAutomaton.empty in

  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (tt, [])) () bool_t aut in
  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (ff, [])) () bool_t aut in

  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (a, [])) () ab_t aut in
  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (b, [])) () ab_t aut in

  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (nil, [])) () list_t aut in
  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (cons, [
      TypeAutomaton.Configuration.Var ab_t;
      TypeAutomaton.Configuration.Var list_t
    ])) () list_t aut
  in
  TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (sorted, [TypeAutomaton.Configuration.Var list_t])) () bool_t aut

let state_type = function
  | MaybeRefinedType.Sample (ty, _) -> ty

let normalizer ty =
  MaybeRefinedType.next_sample ty, ()

let _ =
  let type_system =
    {
      Sampler.automaton = ty_automaton;
      state_type = state_type;
      normalizer = normalizer
    }
  in
  let sampler = Sampler.create type_system in
  let sampler, _ = Sampler.handle (sorted, [list_t]) bool_t sampler in
  let sampler, _ = Sampler.grow sampler in
  let sampler, _ = Sampler.grow sampler in
  let sampler, _ = Sampler.grow sampler in
  Log.print "sampler:\n%t\n" (Sampler.print sampler);
  sampler
