open Timbuk
open TimbukUtils
open TimbukSampler

module TypeAutomaton = Automaton.Make (StringSymbol) (StringState) (NoLabel)

module Type = StringState

module MaybeRefinedType = struct
  type t =
    | Sample of Type.t * int

  let count = ref 0

  let next_id () =
    let id = !count in
    count := id + 1;
    id

  let next_sample ty =
    Sample (ty, next_id ())

  let equal a b =
    begin match a, b with
      | Sample (ta, na), Sample (tb, nb) -> na = nb && Type.equal ta tb
    end

  let compare a b =
    begin match a, b with
      | Sample (ta, na), Sample (tb, nb) ->
        let c = compare na nb in
        if c = 0 then Type.compare ta tb else c
    end

  let hash = Hashtbl.hash

  let product _ _ = failwith "no product defined"

  let print t out =
    begin match t with
      | Sample (ty, n) -> Format.fprintf out "%t#%d" (Type.print ty) n
    end
end

module Sampler = Weighted.Make (StringSymbol) (MaybeRefinedType) (NoLabel) (TypeAutomaton)

let zero = StringSymbol.create "0" 0
let succ = StringSymbol.create "s" 1
let even = StringSymbol.create "even" 1
let nat = StringState.of_string "nat"

let ty_automaton =
  let aut = TypeAutomaton.empty in
  let aut = TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (zero, [])) () nat aut in
  TypeAutomaton.add_transition (TypeAutomaton.Configuration.Cons (succ, [TypeAutomaton.Configuration.Var nat])) () nat aut

let state_type q =
  nat

let normalizer ty =
  MaybeRefinedType.next_sample ty, ()

let _ =
  let type_system =
    {
      Sampler.automaton = ty_automaton;
      state_type = state_type;
      normalizer = normalizer
    }
  in
  let sampler = Sampler.create type_system in
  let sampler, _ = Sampler.handle (even, [nat]) nat sampler in
  let sampler, _ = Sampler.grow sampler in
  let sampler, _ = Sampler.grow sampler in
  let sampler, _ = Sampler.grow sampler in
  Log.print "sampler:\n%t\n" (Sampler.print sampler);
  sampler
