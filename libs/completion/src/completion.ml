open Collections
open Timbuk

module MPattern = Pattern
module MTypedPattern = TypedPattern

module type S = sig
  module Classes : Classes.S
  module Pattern : Pattern.S with type Sym.t = Classes.Aut.Sym.t and type Var.t = Var.t
  module Equation : Equation.S with type elt = Pattern.t
  module Theory : Relation.S with type ord = Pattern.t and type elt = Equation.t
  module rec CriticalPair : sig
    type cp = CP of (TypedPattern.t * TypedPattern.t)
    include Automaton.STATE with type t = cp
  end
  and Label : (Automaton.LABEL with type t = CriticalPair.t option)
  and Aut : (Automaton.S with type Sym.t = Classes.Aut.Sym.t and type State.t = Classes.Class.t and type Label.t = Label.t)
  and TypedPattern : MTypedPattern.S with type Sym.t = Classes.Aut.Sym.t and type Type.t = Aut.Binding.t and type Var.t = Var.t
  module TypedTerm = Aut.BoundTerm
  module TypedRule : Rule.S with type elt = TypedPattern.t
  module Matching : (Matching.S with module Aut = Aut and type Var.t = Pattern.Var.t and type TypedPattern.t = TypedPattern.t)
  module Match = Matching.Match
  module Rule : Rule.S with type elt = Pattern.t
  module Trs : Relation.S with type ord = Pattern.t and type elt = Rule.t
  type t
  type event =
    | Starting of t
    | Step of t * Matching.t
    | FoundMissingTransition of t * Classes.Class.t * Rule.t * Match.t
    | NewTransition of t * Aut.Configuration.t * Label.t * Classes.Class.t
    | Done of t
    (* val classes_of_theory : Theory.t -> Classes.t
       val theory_of_classes : Classes.t -> Theory.t *)
  val complete : ?guard:(t -> bool) -> ?hook:(event -> unit) -> ?fuel:int -> Trs.t -> Classes.t -> Aut.t -> Aut.t
  val create : ?hook:(event -> unit) -> Trs.t -> Classes.t -> Aut.t -> t
  val trs : t -> Trs.t
  val automaton : t -> Aut.t
  val classes : t -> Classes.t
  val step : t -> t
  val completed : t -> bool
  val print_critical_pair : Classes.Class.t -> Rule.t -> Match.t -> Format.formatter -> unit
end

module Label (Ord : Symbol.ORDERED_FORMAT_TYPE) = struct
  type t = Ord.t option

  let compare a b =
    match a, b with
    | Some a, Some b -> Ord.compare a b
    | Some _, None -> 1
    | None, Some _ -> -1
    | None, None -> 0

  let equal a b =
    match a, b with
    | Some a, Some b -> Ord.equal a b
    | None, None -> true
    | _, _ -> false

  let hash = function
    | Some a -> Ord.hash a
    | None -> 0

  let product a b =
    match a, b with
    | Some a, Some b -> if Ord.compare a b = 0 then Some (Some a) else None
    | Some a, None -> Some (Some a)
    | None, Some b -> Some (Some b)
    | None, None -> Some (None)

  let print t out =
    match t with
    | Some rule ->
      Format.fprintf out "[%t]" (Ord.print rule)
    | None -> ()
end

module FLabel = Label

module Make (C : Classes.S) = struct
  module Classes = C

  module Class = Classes.Class

  (** Terms with variables. *)
  module Pattern = Pattern.Make (Classes.Aut.Sym) (Var)
  module PatternMap = Map.Make (Pattern)

  module Equation = Equation.Make (Pattern)
  module Theory = Relation.Make (Pattern) (Equation)

  module rec CriticalPair : sig
    type cp = CP of (TypedPattern.t * TypedPattern.t)

    include Automaton.STATE with type t = cp

    val create : TypedPattern.t -> TypedPattern.t -> t
  end = struct
    type cp = CP of (TypedPattern.t * TypedPattern.t)
    type t = cp

    let create left right =
      CP (left, right)

    let compare (CP (ra, sa)) (CP (rb, sb)) =
      let c = TypedPattern.compare ra rb in
      if c = 0 then TypedPattern.compare sa sb
      else c

    let equal (CP (ra, sa)) (CP (rb, sb)) =
      (TypedPattern.equal ra rb) && (TypedPattern.equal sa sb)

    let hash (CP (r, s)) =
      (TypedPattern.hash r) lxor (TypedPattern.hash s)

    let product (CP (ra, sa)) (CP (rb, sb)) =
      if TypedPattern.compare ra rb = 0 && TypedPattern.compare sa sb = 0
      then Some (CP (ra, sa))
      else None

    let print (CP (left, right)) out =
      Format.fprintf out "[%t -> %t]" (TypedPattern.print left) (TypedPattern.print right)
  end

  and Label : (Automaton.LABEL with type t = CriticalPair.t option) = FLabel (CriticalPair)

  and Aut : (Automaton.S with type Sym.t = Classes.Aut.Sym.t and type State.t = Classes.Class.t and type Label.t = Label.t) = Automaton.Make (Classes.Aut.Sym) (Class) (Label)

  and TypedPattern : (MTypedPattern.S with type Sym.t = Classes.Aut.Sym.t and type Type.t = Aut.Binding.t and type Var.t = Var.t) = MTypedPattern.Make (Classes.Aut.Sym) (Var) (Aut.Binding)

  module TypedTerm = Aut.BoundTerm

  module TypedRule = Rule.Make (TypedPattern)

  module Substitution = TypedPattern.Substitution

  module Matching = Matching.Make (Aut) (Pattern.Var)

  module Match = Matching.Match

  module Rule = Rule.Make (Pattern)
  module Trs = Relation.Make (Pattern) (Rule)

  module VarClassExt = MPattern.Ext (Pattern) (Aut.Configuration)

  type event =
    | Starting of t
    | Step of t * Matching.t
    | FoundMissingTransition of t * Class.t * Rule.t * Match.t
    | NewTransition of t * Aut.Configuration.t * Label.t * Class.t
    | Done of t

  and t = {
    aut : Aut.t;
    (* for completion *)
    trs : Trs.t;
    trs_patterns : Matching.PatternAut.t;
    (* for simplification *)
    classes : Classes.t;
    (* status *)
    completed : bool;
    hook : (event -> unit) option
  }

  let emit event t =
    match t.hook with
    | Some f -> f event
    | None -> ()

  (*	let initial_automaton a = *)
  (*		Auts.map (function x -> x) (Class.of_state) a*)

  let create ?hook trs classes a =
    let t = {
      aut = a;
      trs = trs;
      trs_patterns = Matching.pattern_automaton (Trs.left_sides trs);
      classes = classes;
      completed = false;
      hook = hook
    } in
    emit (Starting t) t;
    t

  let automaton t = t.aut

  let trs t = t.trs

  let classes t = t.classes

  let completed t = t.completed

  let print_critical_pair q rule sigma out =
    Format.fprintf out "<%t, %t, %t>" (Class.print q) (Rule.print rule) (Match.print sigma)

  let fold_critical_pairs matching f t =
    (*		Matching.format_matches (format_binding) (Format.err_formatter) matching;*)
    let do_fold q left m aut =
      let rights = Trs.rights_of left t.trs in
      List.fold_right (f q m left) rights aut
    in
    Matching.fold (do_fold) matching t.aut

  let as_conf_substitution sigma x =
    match Substitution.find x sigma with
    | (Some q, _) -> Aut.Configuration.Var q
    | _ -> raise (Invalid_argument "untyped variable")

  let rec get_typed_right right typed_conf =
    match right, typed_conf with
    | Pattern.Cons (f, l1), (Aut.BoundConfiguration.Cons (_, l2), typ) ->
      TypedPattern.Cons (f, List.map2 get_typed_right l1 l2), typ
    | Pattern.Var x, (Aut.BoundConfiguration.Var _, typ) ->
      TypedPattern.Var x, typ
    | _, _ -> failwith "Completion.get_typed_right: failed normalization."

  (* let rec as_classes_conf = function
    | Aut.Configuration.Cons (f, l) -> Classes.Aut.Configuration.Cons (f, List.map as_classes_conf l)
    | Aut.Configuration.Var q -> Classes.Aut.Configuration.Var q *)

  let step t =
    let normalizer f l =
      let class_conf = Classes.Aut.Configuration.Cons (f, List.map Classes.Aut.Configuration.of_var l) in
      (Classes.class_of class_conf t.classes, None)
    in
    let added_transition_count = ref 0 in
    let hook conf label k = emit (NewTransition (t, conf, label, k)) t; incr added_transition_count in
    let resolve_critical_pair k m left right eqaut =
      if Matching.Match.direct m then begin (* we ignore indirect matches *)
        emit (FoundMissingTransition (t, k, (left, right), m)) t;
        let typed_left = Matching.Match.pattern m in
        let sigma = as_conf_substitution (TypedPattern.substitution typed_left) in
        let conf = VarClassExt.substitute sigma right in
        let typed_conf, eqaut = Aut.add_configuration normalizer ~hook conf eqaut in
        let typed_right = get_typed_right right typed_conf in
        match typed_conf with
        | _, (Some k', _) ->
          (* Now we connect k' to k. *)
          let label = CriticalPair.create typed_left typed_right in
          Aut.add_transition ~hook (Aut.Configuration.Var k') (Some label) k eqaut
        | _, (None, _) -> failwith "Completion.step: failed normalization."
      end else eqaut
    in
    let matching = Matching.create true t.trs_patterns t.aut in
    emit (Step (t, matching)) t;
    let aut = fold_critical_pairs matching resolve_critical_pair t in
    let completed = !added_transition_count = 0 in
    if completed then emit (Done t) t;
    { t with
      aut = aut;
      completed = completed
    }

  (* module Simplifier = struct
     type t = Classes.t

     let create state_classes = state_classes

     let merge hook (k: Class.t) (c: Class.t) t =
      Classes.union ~default:(function x -> x) ~hook k c t

     (** Merge two classes of q1 and q2. *)
     (* let merge hook q1 q2 t = Classes.union ~default:(function q -> Class.of_state q) ~hook q1 q2 t *)

     (** Apply the simplification on the automaton aut. *)
     (* let apply aut t =
       let map_state k =
        match k with
        | Class.Dummy -> Class.Dummy
        | Class.Group states -> Classes.find ~default:(function _ -> Class.Dummy) (StateSet.choose states) t
       in
       let map_label = function
        | Some (rule, sigma) -> Some (rule, Substitution.map (map_state) sigma)
        | None -> None
       in
       Automaton.map (map_label) (map_state) aut, t *)

     let apply hook aut t =
      let map_state k =
        let default_class = function
          | Class.Rest -> Class.Rest
          | Class.Class c -> Class.Class c
          | singleton ->
            hook singleton Class.Rest;
            Class.Rest
        in
        Classes.find ~default:default_class k t
      in
      let rec map_type = function
        | (Some q, Some label) -> (Some (map_state q), Some (map_label label))
        | (Some q, None) -> (Some (map_state q), None)
        | _ -> failwith "untyped completion label"
      and map_pattern = function
        | BasicTypedPattern.Cons (f, l), typ -> BasicTypedPattern.Cons (f, List.map map_pattern l), (map_type typ)
        | BasicTypedPattern.Var x, typ -> BasicTypedPattern.Var x, (map_type typ)
        | BasicTypedPattern.Cast tp, typ ->
          begin
            match map_pattern tp, map_type typ with
            | (tp, (Some q1, Some label1)), (Some q2, Some label2) ->
              if Class.equal q1 q2 then
                tp, (Some q1, Some label1)
              else
                BasicTypedPattern.Cast (tp, (Some q1, Some label1)), (Some q2, Some label2)
            | tp, _ ->
              let ppf = Format.std_formatter in
              Format.pp_print_string ppf "untyped2: \n";
              BasicTypedPattern.format ppf tp;
              Format.pp_print_string ppf "\n";
              failwith "untyped completion label"
          end
      and map_label = function
        | Some (CriticalPair.CP (rule, tpattern)) -> Some (CriticalPair.CP (rule, map_pattern tpattern))
        | None -> None
      in
      Aut.map map_label map_state aut, t
     end *)

  (* let simplify t =
     match t.classes with
     | Some classes ->
      emit (Simplification (t, classes)) t;
      (* First we compute the product automaton of the current A and the classes.
         This gives us an automaton where each state is a pair (k, class).
         In this case this means that the state [k] recognizes a term of [class].
         So we need to merge this state with the rest of the class. *)
      let label_prod _ () = Some () in
      let class_prod k c = Some (k, c) in
      let nrprod = (ClassProduct.product (label_prod) (class_prod) t.aut classes) in
      let prod = ClassAut.reduce ~epsilon:false nrprod in
      (* let ppf = Format.std_formatter in
         Format.pp_print_string ppf "\n============\n";
         Automaton.format ppf t.aut;
         Format.pp_print_string ppf "\n============\n";
         ClassAutomaton.format ppf nrprod;
         Format.pp_print_string ppf "\n============\n";
         ClassAutomaton.format ppf prod;
         Format.pp_print_string ppf "============\n"; *)
      (* We count how many merging we'v done, to know when the automaton is completed. *)
      let merging_count = ref 0 in
      (* Now we process all pair (k, class) to merge [k] with the rest of [class]. *)
      let on_merge c c1 c2 =
        emit (Merging (t, c, c1, c2)) t;
        merging_count := !merging_count + 1
      in
      let set_class ((k, c) : Class.t * Classes.State.t) simp =
        Simplifier.merge (on_merge (Some c)) k (Class.of_class c) simp
      in
      let simp = ClassAut.fold_states (set_class) prod (Simplifier.create t.state_classes) in
      (* This gives us a 'simplifier', that we can apply on the current automaton. *)
      let aut, state_classes = Simplifier.apply (on_merge None) t.aut simp in
      (* If no state has been merged (merging_count = 0) and no completion step has been done (t.completed),
         then the automaton is completed. *)
      let completed = t.completed && (!merging_count = 0) in
      (* If it is complted, we emit the corresponding signal. *)
      if completed then emit (Done t) t else ();
      (* Finaly, we return the current state. *)
      {	t with
        aut = aut;
        state_classes = state_classes;
        completed = completed
      }
     | None -> t *)

  (* let theory_of_classes classes =
     Theory.empty (* TODO *)

     let classes_of_theory thy =
     Classes.empty (* TODO *) *)

  let complete ?guard ?hook ?fuel trs classes a =
    let rec do_complete ?fuel current =
      if completed current then current else
        let continue = match guard with
          | Some g -> g current
          | None -> true
        in
        if continue then
          match fuel with
          | Some i -> if i <= 0 then current else do_complete ~fuel:(i-1) (step current)
          | None -> do_complete (step current)
        else
          current
    in
    automaton (do_complete ?fuel (create ?hook trs classes a))
end
