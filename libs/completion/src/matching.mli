open Timbuk

module type S = sig
  module Aut : Automaton.S

  module Var : Pattern.VARIABLE

  (** Typed terms *)
  module TypedTerm = Aut.BoundTerm

  (** State *)
  module State = Aut.State

  (** Label *)
  module Label = Aut.Label

  (** Terms with variables. *)
  module TypedPattern : TypedPattern.S with type Sym.t = Aut.Sym.t and type Var.t = Var.t and type Type.t = Aut.Binding.t

  module Pattern : Pattern.S with type Sym.t = Aut.Sym.t and type Var.t = Var.t

  module Matching : Automaton.STATE with type t = Pattern.t * State.t

  module PatternAut : Automaton.S with type Sym.t = Aut.Sym.t and type State.t = Pattern.t and type Label.t = unit

  module MatchingAut : Automaton.S with type Sym.t = Aut.Sym.t and type State.t = Matching.t and type Label.t = Label.t

  module Match : sig
    type t

    val pattern : t -> TypedPattern.t

    val direct : t -> bool

    (* val product : t -> t -> t option *)

    val compare : t -> t -> int

    val print : t -> Format.formatter -> unit
  end

  type t

  (** Creates a pattern automaton from a list of patterns. *)
  val pattern_automaton : Pattern.t list -> PatternAut.t

  (** Perform matching. *)
  val create : ?overmatching:bool -> bool -> PatternAut.t -> Aut.t -> t

  val automaton : t -> MatchingAut.t

  (*	val update : Aut.t -> Aut.Delta.t -> t -> t*)

  val fold : (State.t -> Pattern.t -> Match.t -> 'a -> 'a) -> t -> 'a -> 'a

  (* val fold2 : (State.t -> Pattern.t -> State.t -> Pattern.t -> Match.t -> 'a -> 'a) -> t -> t -> 'a -> 'a *)

  val iter : (State.t -> Pattern.t -> Match.t -> unit) -> t -> unit

  val print : t -> Format.formatter -> unit

  val print_matches : t -> Format.formatter -> unit
end

module Make (A : Automaton.S) (X : Pattern.VARIABLE)
  : S with module Aut = A and module Var = X
