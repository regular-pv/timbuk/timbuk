open Timbuk

module MTypedPattern = TypedPattern

module type S = sig
  module Classes : Classes.S

  module Pattern : Pattern.S with type Sym.t = Classes.Aut.Sym.t and type Var.t = Var.t

  module Equation : Equation.S with type elt = Pattern.t
  module Theory : Relation.S with type ord = Pattern.t and type elt = Equation.t

  module rec CriticalPair : sig
    type cp = CP of (TypedPattern.t * TypedPattern.t)

    include Automaton.STATE with type t = cp
  end

  and Label : (Automaton.LABEL with type t = CriticalPair.t option)

  and Aut : (Automaton.S with type Sym.t = Classes.Aut.Sym.t and type State.t = Classes.Class.t and type Label.t = Label.t)

  and TypedPattern : MTypedPattern.S with type Sym.t = Classes.Aut.Sym.t and type Type.t = Aut.Binding.t and type Var.t = Var.t

  module TypedTerm = Aut.BoundTerm

  module TypedRule : Rule.S with type elt = TypedPattern.t

  module Matching : (Matching.S with module Aut = Aut and type Var.t = Pattern.Var.t and type TypedPattern.t = TypedPattern.t)

  module Match = Matching.Match

  module Rule : Rule.S with type elt = Pattern.t

  module Trs : Relation.S with type ord = Pattern.t and type elt = Rule.t

  type t

  type event =
    | Starting of t
    | Step of t * Matching.t
    | FoundMissingTransition of t * Classes.Class.t * Rule.t * Match.t
    | NewTransition of t * Aut.Configuration.t * Label.t * Classes.Class.t
    | Done of t

  (* val classes_of_theory : Theory.t -> Classes.t

     val theory_of_classes : Classes.t -> Theory.t *)

  (** Generate an initial automaton for completion from a regular automaton. *)
  (*	val initial_automaton : Aut.t -> Aut.t*)

  (** Generic completion function. *)
  val complete : ?guard:(t -> bool) -> ?hook:(event -> unit) -> ?fuel:int -> Trs.t -> Classes.t -> Aut.t -> Aut.t

  (** Create a new completion environment. *)
  val create : ?hook:(event -> unit) -> Trs.t -> Classes.t -> Aut.t -> t

  (** The TRS used for completion. *)
  val trs : t -> Trs.t

  (** The current automaton. *)
  val automaton : t -> Aut.t

  val classes : t -> Classes.t

  (** Perform one step of completion. *)
  val step : t -> t

  (** True when completion finishes. *)
  val completed : t -> bool

  val print_critical_pair : Classes.Class.t -> Rule.t -> Match.t -> Format.formatter -> unit
end

module Make (C : Classes.S) : sig
  include S with module Classes = C
end
