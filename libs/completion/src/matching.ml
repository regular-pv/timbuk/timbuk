open Collections
open Timbuk

module type S = sig
  module Aut : Automaton.S
  module Var : Pattern.VARIABLE
  module TypedTerm = Aut.BoundTerm
  module State = Aut.State
  module Label = Aut.Label
  module TypedPattern : TypedPattern.S with type Sym.t = Aut.Sym.t and type Var.t = Var.t and type Type.t = Aut.Binding.t
  module Pattern : Pattern.S with type Sym.t = Aut.Sym.t and type Var.t = Var.t
  module Matching : Automaton.STATE with type t = Pattern.t * State.t
  module PatternAut : Automaton.S with type Sym.t = Aut.Sym.t and type State.t = Pattern.t and type Label.t = unit
  module MatchingAut : Automaton.S with type Sym.t = Aut.Sym.t and type State.t = Matching.t and type Label.t = Label.t
  module Match : sig
    type t
    val pattern : t -> TypedPattern.t
    val direct : t -> bool
    val compare : t -> t -> int
    val print : t -> Format.formatter -> unit
  end
  type t
  val pattern_automaton : Pattern.t list -> PatternAut.t
  val create : ?overmatching:bool -> bool -> PatternAut.t -> Aut.t -> t
  val automaton : t -> MatchingAut.t
  val fold : (State.t -> Pattern.t -> Match.t -> 'a -> 'a) -> t -> 'a -> 'a
  val iter : (State.t -> Pattern.t -> Match.t -> unit) -> t -> unit
  val print : t -> Format.formatter -> unit
  val print_matches : t -> Format.formatter -> unit
end

module Make (A : Automaton.S) (X : Pattern.VARIABLE) = struct
  module Aut = A
  module Var = X
  module TypedTerm = Aut.BoundTerm
  module State = Aut.State
  module Label = Aut.Label
  module Configuration = Aut.Configuration

  module TypedPattern = TypedPattern.Make (Aut.Sym) (Var) (Aut.Binding)
  module Pattern = Pattern.Make (Aut.Sym) (Var)

  module Matching = struct
    type t = Pattern.t * State.t

    let create p q = (p, q)

    let state (_, q) = q

    let compare (p1, q1) (p2, q2) =
      let c = Pattern.compare p1 p2 in
      if c = 0 then State.compare q1 q2 else c

    let print (p, q) out =
      Format.fprintf out "[%t, %t]" (Pattern.print p) (State.print q)

    let product _ _ = None

    let equal (p1, q1) (p2, q2) =
      (Pattern.equal p1 p2) && (State.equal q1 q2)

    let hash (p, q) =
      (Pattern.hash p) lxor (State.hash q)
  end

  module PatternAut = Automaton.Make (Aut.Sym) (Pattern) (NoLabel)

  module MatchingAut = Automaton.Make (Aut.Sym) (Matching) (Label)

  module Prod = Automaton.Product (PatternAut) (Aut) (MatchingAut)

  module Match = struct
    type t = {
      pattern: TypedPattern.t;
      direct: bool (* if the top transition is not, or is, epsilon. *)
    }

    let create direct pattern = {
      pattern = pattern;
      direct = direct
    }

    let pattern t = t.pattern

    let direct t = t.direct

    let compare a b =
      let c = compare a.direct b.direct in
      if c = 0 then TypedPattern.compare a.pattern b.pattern else c

    let print t = TypedPattern.print t.pattern

    let set_direct d t = { t with direct = d }
  end

  module MatchingMap = Map.Make (Matching)

  module MatchSet = Set.Make (Match)

  type t = {
    epsilon : bool;
    overmatching : bool;
    patterns : PatternAut.t;
    matching : MatchingAut.t;
    aut : Aut.t;
    map : MatchSet.t MatchingMap.t
  }

  let automaton t = t.matching

  let matches q t =
    match MatchingMap.find_opt q t.map with
    | Some set -> set
    | None -> MatchSet.empty

  (* let ppf = Format.err_formatter

     let debug str =
     Format.pp_print_string ppf str;
     Format.pp_force_newline ppf ();
     Format.pp_print_flush ppf () *)

  let rec configuration_matches ?added typ direct conf t =
    match conf with
    | MatchingAut.Configuration.Cons (f, l) ->
      let rec fold_combinations (g: TypedPattern.t list -> MatchSet.t -> MatchSet.t) l (x:MatchSet.t) =
        match l with
        | [] -> g [] x
        | conf::l ->
          begin
            let matches : MatchSet.t = configuration_matches (None, None) true conf t in
            let for_each_match m (x:MatchSet.t) =
              let p = Match.pattern m in
              let for_each_sub_combination l' (x:MatchSet.t) =
                (* TODO: make sure that p is compatible with l': all variables are typed the same. *)
                g (p::l') x
              in
              fold_combinations for_each_sub_combination l x
            in
            MatchSet.fold for_each_match matches x
          end
      in
      let match_of_combination l (matches:MatchSet.t) =
        let pattern = TypedPattern.Cons (f, l), typ in
        let m = Match.create true pattern in
        MatchSet.add m matches
      in
      fold_combinations match_of_combination l MatchSet.empty
    | MatchingAut.Configuration.Var m' ->
      begin
        match added with
        | Some (m, added) when Matching.compare m' m = 0 ->
          MatchSet.map (Match.set_direct direct) added
        | _ -> matches m' t
      end
  (* in
     if direct then matches else
     (* If there is an epsilon transition, we add a type cast. *)
     MatchSet.map (Match.wrap_in_cast typ) matches *)

  (** Add a new substitution possible for matching m *)
  let rec add match_set (m : Matching.t) t =
    (* Note: we name 'host' of a state m any configuration containing m. Including m itself. *)
    (* STEP 1. We try to add the substitution to state m. *)
    let added = ref MatchSet.empty in (* store the effectively added substitutions. *)
    let on_add m = function
      | Some _ -> ()
      | None -> added := MatchSet.add m !added
    in
    let try_add m = (* try to add the substitution. On success, 'on_add' is called. *)
      MatchSet.update (on_add m) m
    in
    let update_matches = function
      (* We had some previous matches. Try add the new matches one by one. *)
      | Some set -> Some (MatchSet.fold (try_add) match_set set)
      (* No previous matches. Add them all. *)
      | None -> added := match_set; Some (match_set)
    in
    (* updating matches in m in t.map. *)
    let map = MatchingMap.update m (update_matches) t.map in
    (* NOTE: at this point, [added] contains all the matches effectively added to m. *)

    (* STEP 2. We propagete the patterns to the parents state. *)
    let t = { t with map = map } in
    let add_to_host host t =
      (* Note that if we have Var, then my host is myself. *)
      match host with
      | MatchingAut.Configuration.Var _ when not t.epsilon -> t
      | _ ->
        (* We compute the new matches for the parent states. *)
        let new_matches typ =
          configuration_matches ~added:(m, !added) typ false host t
        in
        let do_add_to_parent (m', label) =
          add (new_matches (Some (Matching.state m'), Some label)) m'
        in
        let parents = MatchingAut.states_for_configuration host t.matching in
        MatchingAut.LabeledStateSet.fold do_add_to_parent parents t
    in
    (* we propagate only if at least one substitution has been added. *)
    if MatchSet.is_empty !added then t else
      let hosts = MatchingAut.state_parents m t.matching in
      MatchingAut.ConfigurationSet.fold (add_to_host) hosts t

  let fold_states f t value =
    let fold_pattern p value =
      Aut.fold_states (f p) t.aut value
    in
    PatternAut.fold_states (fold_pattern) t.patterns value

  let initialize t =
    let init_state p q t =
      let m = p, q in
      let t = match m with
        | (Pattern.Var x, q) ->
          add (MatchSet.singleton (Match.create true (TypedPattern.Var x, (Some q, None)))) m t
        | _, _ -> t
      in
      let init_transition (conf, label) t =
        let matches = configuration_matches (Some q, Some label) false conf t in
        add matches m t
      in
      let confs = MatchingAut.configurations_for_state m t.matching in
      MatchingAut.LabeledConfigurationSet.fold (init_transition) confs t
    in
    fold_states init_state t t

  let pattern_automaton plist =
    let rec configuration_of_pattern = function
      | Pattern.Cons (f, l) ->
        PatternAut.Configuration.Cons (f, List.map (configuration_of_pattern) l)
      | Pattern.Var x ->
        PatternAut.Configuration.Var (Pattern.Var x)
    in
    (* let rec pattern_of_configuration = function
      | PatternAut.Configuration.Cons (f, l) ->
        Pattern.Cons (f, List.map (pattern_of_configuration) l)
      | PatternAut.Configuration.Var p -> p
    in *)
    let labeled_pattern_of_configuration f l = (Pattern.Cons (f, l)), () in
    let add aut pattern = snd (PatternAut.add (labeled_pattern_of_configuration) (configuration_of_pattern pattern) aut) in
    List.fold_left (add) PatternAut.empty plist

  let create ?(overmatching=false) epsilon patterns aut =
    let label_product () label = Some label in
    let matching_product a b = Some (Matching.create a b) in
    let m = Prod.product (fun _ _ -> ()) (label_product) (matching_product) patterns aut in
    initialize {
      overmatching = overmatching;
      epsilon = epsilon;
      patterns = patterns;
      matching = m;
      aut = aut;
      map = MatchingMap.empty
    }

  let print t =
    MatchingAut.print t.matching

  let fold f t value =
    let fold_pattern p value =
      let fold_state q value =
        MatchSet.fold (f q p) (matches (p, q) t) value
      in
      Aut.fold_states (fold_state) t.aut value
    in
    PatternAut.StateSet.fold (fold_pattern) (PatternAut.final_states t.patterns) value

  (* let fold2 f t1 t2 value =
     let fold2' q1 p1 match1 value =
      let do_fold q2 p2 match2 value =
        match Match.product match1 match2 with
        | Some m -> f q1 p1 q2 p2 m value
        | None -> value
      in
      fold (do_fold) t2 value
     in
     fold (fold2') t1 value *)

  let iter f t =
    fold (fun q p s () -> f q p s) t ()

  let print_matches t out =
    let print_match q p s =
      Format.fprintf out "<%t, %t, %t>\n" (State.print q) (Pattern.print p) (Match.print s)
    in
    iter print_match t
end
