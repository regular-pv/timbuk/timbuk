open Timbuk

(* Implicit term classifier.  *)
module type S = sig
  module Class : Automaton.STATE
  module Aut : Automaton.S with type State.t = Class.t and type Label.t = unit

  type t

  val class_of : Aut.Configuration.t -> t -> Class.t
  (** [class_of conf t] Decide the class for a given normalized configuration.
      raise Invalid_argument if the configuration is not normalized,
      or if it is not recognized by any class. *)

  val count : t -> int

  val print : t -> Format.formatter -> unit
end

module type TEMPLATE = sig
  module Template : Automaton.S
  include S with type Class.t = Template.State.t * Var.t and type Aut.Sym.t = Template.Sym.t

  val create : Template.t -> Aut.t -> t
  (** [create T A]
      Create classes from a template automaton T and a "subdivision" automaton A.
      both automata must be deterministic.
      the "subdivision" automaton must match the template automaton in the sense that
      if we have f(@x1.y1, ..., @xn.yn) -> (@x.y) in A then we must have
      f(@x1, ..., @xn) -> @x in T. In other words, A is a "subdivision" of T.
      However we may have L(A) ⊂ L(T).
      NOTE: for the completion algorithm, T does not have to be complete,
      but have to recognize at least R/E*(L) where L is the initial language.
      In practice it can recognize only the well typed terms. *)
end

module Make (Template : Automaton.S) :
  TEMPLATE with module Template = Template

module Coherence (Classes : S) (Aut : Automaton.S with type Sym.t = Classes.Aut.Sym.t) (Coherent : Automaton.S with type Sym.t = Classes.Aut.Sym.t and type State.t = Classes.Class.t and type Label.t = Aut.Label.t) : sig
  val make_coherent : Classes.t -> Aut.t -> Coherent.data -> Coherent.t
  (** Make an automaton coherent w.r.t given classes.
      The input automaton must be normalized and epsilon-free. *)
end
