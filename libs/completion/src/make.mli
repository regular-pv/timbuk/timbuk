module Env (Classes : Classes.S) : sig
  (* module Completion : Completion.S with module Term = Aut.Term and module State = Aut.State and module Classes = Classes *)
  module Completion : module type of Completion.Make (Classes)
	module Rule = Completion.Rule
	module Trs = Completion.Trs
	module CompletedAutomaton = Completion.Aut
end
