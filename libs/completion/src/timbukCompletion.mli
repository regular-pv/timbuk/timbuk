(** Timbuk Completion

    Algorithms for tree automata completion.
*)

(** {1 Pattern matching} *)

module Matching = Matching

(** {1 Completion} *)

module Completion = Completion

module Classes = Classes

(** {1 Other} *)

module Make = Make
