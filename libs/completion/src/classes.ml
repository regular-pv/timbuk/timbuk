open Collections
open Timbuk

module type S = sig
  module Class : Automaton.STATE
  module Aut : Automaton.S with type State.t = Class.t and type Label.t = unit
  type t
  val class_of : Aut.Configuration.t -> t -> Class.t
  val count : t -> int
  val print : t -> Format.formatter -> unit
end

module type TEMPLATE = sig
  module Template : Automaton.S
  include S with type Class.t = Template.State.t * Var.t and type Aut.Sym.t = Template.Sym.t
  val create : Template.t -> Aut.t -> t
end

module Make (Template : Automaton.S) = struct
  module Sym = Template.Sym

  module Class = struct
    type t = Template.State.t * Var.t

    let product (qa, xa) (qb, xb) =
      match Template.State.product qa qb, Var.product xa xb with
      | Some q, Some x -> Some (q, x)
      | _ -> None

    let compare (qa, xa) (qb, xb) =
      let c = Template.State.compare qa qb in
      if c = 0 then Var.compare xa xb else c

    let equal (qa, xa) (qb, xb) =
      Template.State.equal qa qb && Var.equal xa xb

    let hash (q, x) =
      (Template.State.hash q) lxor (Var.hash x)

    let print (q, x) out =
      Format.fprintf out "%t.%t" (Template.State.print q) (Var.print x)
  end

  module Aut = Automaton.Make (Sym) (Class) (NoLabel)

  module Template = Template

  type t = {
    template : Template.t;
    aut: Aut.t
  }

  let rec unsplit = function
    | Aut.Configuration.Cons (f, l) -> Template.Configuration.Cons (f, List.map unsplit l)
    | Aut.Configuration.Var (q, _) -> Template.Configuration.Var q

  let class_of conf t =
    let states = Aut.states_for_configuration conf t.aut in
    if Aut.LabeledStateSet.cardinal states > 1 then
      failwith "more than one class for a configuration.";
    match Aut.LabeledStateSet.choose_opt states with
    | Some ((q, x), _) -> q, x
    | None ->
      begin
        let template_conf = unsplit conf in
        match Template.LabeledStateSet.choose_opt (Template.states_for_configuration template_conf t.template) with
        | Some (q, _) ->
          let x = Var.of_int 0 in
          q, x
        | None ->
          Format.eprintf "to classify: %t@." (Aut.Configuration.print conf);
          raise (Invalid_argument "configuration cannot be classified")
      end

  let count t =
    (Template.state_count t.template) * (Aut.state_count t.aut)

  let create template aut =
    {
      template = template;
      aut = aut
    }

  let print t out =
    Aut.print t.aut out
end

let combinations (type t) (type elt) (module Set : Set.S with type elt = elt and type t = t) (f : elt list -> 'a -> 'a) (l : t list) (x : 'a) =
  let rec fold_combinations left l x =
    match l with
    | [] -> f (List.rev left) x
    | set::l ->
      let elements = Set.elements set in
      let fold e x =
        fold_combinations (e::left) l x
      in
      List.fold_right fold elements x
  in
  fold_combinations [] l x

module Coherence
    (Classes : S)
    (Aut : Automaton.S with type Sym.t = Classes.Aut.Sym.t)
    (Coherent : Automaton.S with type Sym.t = Classes.Aut.Sym.t and type State.t = Classes.Class.t and type Label.t = Aut.Label.t)
= struct
  let make_coherent classes aut data =
    let module Hashtbl = Hashtbl.Make (Aut.State) in
    let table = Hashtbl.create 8 in
    let normalized = function
      | Aut.Configuration.Cons _ -> raise (Invalid_argument "automaton must be normalized")
      | Aut.Configuration.Var p -> p
    in
    let rec add_class k q caut =
      let k = match Hashtbl.find_opt table q with
        | Some q_classes ->
          if Classes.Aut.StateSet.mem k q_classes then
            None
          else
            begin
              Hashtbl.add table q (Classes.Aut.StateSet.add k q_classes);
              Some k
            end
        | None ->
          Hashtbl.add table q (Classes.Aut.StateSet.singleton k);
          Some k
      in
      match k with
      | Some k ->
        begin
          let sites = Aut.state_parents q aut in
          let fold_call_site conf caut =
            match conf with
            | Aut.Configuration.Cons (f, l) ->
              let rec get_combinations = function
                | [] -> Some []
                | p::l ->
                  let p_classes = if Aut.State.equal p q then
                      Some (Classes.Aut.StateSet.singleton k)
                    else
                      Hashtbl.find_opt table p
                  in
                  match p_classes with
                  | Some p_classes ->
                    begin
                      match get_combinations l with
                      | Some l -> Some (p_classes::l)
                      | None -> None
                    end
                  | None -> None
              in
              let parents = Aut.states_for_configuration conf aut in
              begin
                let l = List.map normalized l in
                match get_combinations l with
                | Some l_classes ->
                  let fold_combination l caut =
                    let kconf = Classes.Aut.Configuration.Cons (f, List.map (function q -> Classes.Aut.Configuration.Var q) l) in
                    let k = Classes.class_of kconf classes in
                    let cconf = Coherent.Configuration.Cons (f, List.map (function q -> Coherent.Configuration.Var q) l) in
                    let fold_parent (q, label) caut =
                      let caut = Coherent.add_transition cconf label k caut in
                      add_class k q caut
                    in
                    Aut.LabeledStateSet.fold fold_parent parents caut
                  in
                  combinations (module Classes.Aut.StateSet) fold_combination l_classes caut
                | None -> caut
              end
            | Aut.Configuration.Var _ ->
              raise (Invalid_argument "automaton must be epsilon-free")
          in
          Aut.ConfigurationSet.fold fold_call_site sites caut
        end
      | None -> caut
    in
    let fold_transition conf label q caut =
      match conf with
      | Aut.Configuration.Cons (f, []) ->
        begin
          let kconf = Classes.Aut.Configuration.Cons (f, []) in
          let k = Classes.class_of kconf classes in
          let cconf = Coherent.Configuration.Cons (f, []) in
          let caut = Coherent.add_transition cconf label k caut in
          add_class k q caut
        end
      | _ -> caut
    in
    let caut = Aut.fold_transitions fold_transition aut (Coherent.create data) in
    let add_final k caut =
      Coherent.add_final_state k caut
    in
    let add_finals q caut =
      match Hashtbl.find_opt table q with
      | Some q_classes ->
        Classes.Aut.StateSet.fold add_final q_classes caut
      | None -> caut
    in
    Aut.StateSet.fold add_finals (Aut.final_states aut) caut
end
