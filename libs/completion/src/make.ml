module Env (Classes : Classes.S) = struct
  module Completion = Completion.Make (Classes)
	module Rule = Completion.Rule
	module Trs = Completion.Trs
	module CompletedAutomaton = Completion.Aut
end
