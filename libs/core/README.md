# Timbuk 4

Timbuk is a tool designed to compute or over-approximate sets of terms
reachable by a given term rewriting system. The libray also provides an
OCaml toplevel with all usual functions on Bottom-up Nondeterministic 
Tree Automata.

## Installation

### Dependencies

Timbuk 4 needs a SMT solver to run properly.
For now, only CVC4 is supported.

- Archlinux (pacman)

    Archlinux provides a community package for CVC4. Just type:
    ```
    $ pacman -S cvc4
    ```

- Mac OS (homebrew)

    I'm not sure if CVC4 appears in the default repository sources.
    However there is an official [homebrew tap](https://github.com/CVC4/homebrew-cvc4) from which you can install CVC4 by typing:
    ```
    $ brew tap cvc4/cvc4
    $ brew install cvc4/cvc4/cvc4
    ```

### Download

Clone the GIT repository from Inria's GitLab:

```
$ git clone git@gitlab.inria.fr:timbuk/timbuk.git
```

### Build

Timbuk 4 is composed of a main OCaml library located in the root directory,
and some command line tools, located in the `tools` directory.

To build the main library go to the root directory and type.

```
$ make
```

To build the command line tools:

```
$ cd tools/
$ make
```

## Usage

### Command line interface (CLI)

```
$ cd tools/
$ ./cmd my_timbuk_file.timbuk
```

You can get a little bit of help by typing:

```
$ ./cmd --help
```

A more detailed introduction of the CLI is available [here](https://gitlab.inria.fr/timbuk/timbuk/wikis/cli).