open Collections
open Timbuk
open TimbukSolving
open TimbukTyping
open TimbukCompletion

let log_src = Logs.Src.create "timbuk.fixpoint-learning.completion"
module Log = (val Logs.src_log log_src : Logs.LOG)

let (>>) f g x =
  g (f x)

module Make
    (Location : sig type t end)
    (TypeSystem : RefinedTypeSystem.S)
    (RegularTypePartition : Set.S with type elt = TypeSystem.TypeAut.StateSet.t)
    (AbsType : Automaton.TYPE with type t = (TypeSystem.TypeAut.State.t, RegularTypePartition.t) Regular.abs_type)
    (AbsTypedPattern : TypedPattern.S with type Sym.t = TypeSystem.TypeAut.Sym.t and type Type.t = AbsType.t * Location.t)
    (AbsTypedTrs : Relation.S with type ord = AbsTypedPattern.t)
    (SolvedAut : Automaton.S with type Sym.t = TypeSystem.TypeAut.Sym.t and type State.t = (TypeSystem.TypeAut.State.t, RegularTypePartition.t) Regular.solved_state and type Label.t = unit and type data = unit)
    (Solver : Solver.S with type Var.t = int) =
struct
  module Location = Location

  module TypeAut = TypeSystem.TypeAut
  module Type = TypeAut.State

  exception IrreducibleNonValue of TypeSystem.TypeAut.Sym.t Term.term

  module Id = struct
    type t = int

    let compare a b = a - b

    let equal a b = a = b

    let hash t = t
  end

  module Node = struct
    type t =
      | Known of AbsType.t * Type.t
      | Unknown of AbsType.t * int

    let count = ref 0

    let next_id () =
      let id = !count in
      count := id+1;
      id

    let is_unknown = function
      | Known _ -> false
      | Unknown _ -> true

    let create_from_abstract abs_ty =
      Unknown (abs_ty, next_id ())

    let abs_ty = function
      | Known (abs_ty, _) -> abs_ty
      | Unknown (abs_ty, _) -> abs_ty

    let product _ _ =
      failwith "Undefined product."

    let print t out =
      match t with
      | Known (abs_ty, q) ->
        Format.fprintf out "%t:%t" (Type.print q) (AbsType.print abs_ty)
      | Unknown (abs_ty, i) ->
        Format.fprintf out "%t@%d" (AbsType.print abs_ty) i

    let compare a b =
      match a, b with
      | Known (a, ta), Known (b, tb) ->
        (* Type.compare ta tb *)
        let c = Type.compare ta tb in
        if c = 0 then AbsType.compare a b else c
      | Known _, _ -> 1
      | _, Known _ -> -1
      | Unknown (a, ia), Unknown (b, ib) ->
        let c = ia - ib in
        if c = 0 then AbsType.compare a b else c

    (* Try to compare known nodes.
       If at least one of the two nodes is not known, then return None.*)
    let try_compare a b =
      (* debug "\nCOMPARE %t x %t\n@." (print a) (print b); *)
      match a, b with
      (*
        | Known ((Regular.Known partition_a, _), ta), Known ((Regular.Known partition_b, _), tb) ->
         (* debug "\nHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA %t x %t\n@." (print a) (print b); *)
         let c = Type.compare ta tb in
         if c = 0 then Some 0 else
          begin
            let ty_a = RegularTypePartition.search (TypeAut.StateSet.mem ta) partition_a in
            let ty_b = RegularTypePartition.search (TypeAut.StateSet.mem tb) partition_b in
            Some (TypeAut.StateSet.compare ty_a ty_b)
          end
*)
      | Known (a, ta), Known (b, tb) ->
        let cmp = let c = Type.compare ta tb in
          if c = 0 then AbsType.compare a b else c in
        Some cmp
      | Unknown ((Regular.Known _, _), _), Known (_, _)
      | Known (_, _), Unknown ((Regular.Known _, _), _) ->
        failwith "comparing unreduced node"
      | Unknown ((Regular.Unknown, _), _), Known _ ->
        Some (-1)
      | Known _, Unknown ((Regular.Unknown, _), _) ->
        Some 1
      | Unknown (_, ia), Unknown (_, ib) when ia = ib ->
        Some 0
      | _, _ ->
        None

    let same_abstraction a b =
      match a, b with
      | Known (abs_ty, _), Known (abs_ty', _) ->
        AbsType.equal abs_ty abs_ty'
      | Unknown (abs_ty, _), Unknown (abs_ty', _) ->
        AbsType.equal abs_ty abs_ty'
      | _, _ ->
        false

    let equal a b =
      match a, b with
      | Known (a, ta), Known (b, tb) ->
        Type.equal ta tb && AbsType.equal a b
      | Unknown (a, ia), Unknown (b, ib) ->
        ia = ib && AbsType.equal a b
      | _, _ -> false

    let hash = function
      | Known (a, t) ->
        (AbsType.hash a) lxor (Type.hash t)
      | Unknown (a, i) ->
        (AbsType.hash a) lxor i
  end

  module AbsAut = Automaton.Make (TypeAut.Sym) (AbsType) (NoLabel)
  module Aut = TimbukSampling.Weighted.Make (TypeAut.Sym) (Node) (NoLabel) (AbsAut)
  (* module Aut = Automaton.Make (TypeAut.Sym) (Node) (NoLabel) *)
  module MapAut = Automaton.Ext (Aut) (SolvedAut)
  module Type2Abs = Automaton.Ext (TypeAut) (AbsAut)

  module NodeSet = Aut.StateSet

  module Matching = Matching.Make (SolvedAut) (AbsTypedPattern.Var)
  module SolvedAutPatterns = SolvedAut.BoundPatterns (AbsTypedPattern.Var)

  module TypeAutExt = Automaton.Ext (TypeAut) (Aut)

  module AbsTypedNodeConfiguration = TypedPattern.Make (TypeAut.Sym) (Node) (AbsType)

  module Pattern = Pattern.Make (TypeAut.Sym) (AbsTypedPattern.Var)
  module StrippedRule = Rule.Make (Pattern)
  module StrippedTrs = Relation.Make (Pattern) (StrippedRule)

  module Seed = struct
    type t = TypeAut.Sym.t * AbsType.t list * AbsType.t

    let of_transition conf node =
      begin match conf with
        | Aut.Configuration.Cons (f, l) ->
          let l = List.map (Aut.Configuration.normalized >> Node.abs_ty) l in
          let ty = Node.abs_ty node in
          Some (f, l, ty)
        | _ ->
          None
      end

    let compare (fa, la, tya) (fb, lb, tyb) =
      let c = AbsAut.Sym.compare fa fb in
      if c = 0 then
        let c = AbsType.compare tya tyb in
        if c = 0 then
          List.compare AbsType.compare la lb
        else c
      else c

    let print ((f, l, ty) : t) fmt =
      Format.fprintf fmt "%t(%t) : %t"
        (TypeAut.Sym.print f)
        (List.print AbsType.print ", " l)
        (AbsType.print ty)
  end

  module SeedSet = Set.Make (Seed)

  type t = {
    type_system : TypeSystem.t;

    trs: StrippedTrs.t;

    abs_typed_trs : AbsTypedTrs.t;

    trs_patterns: Matching.PatternAut.t;
    (** Left-hand side matching of each TRS rule.
        Used to find the critical pair at each completion step, and resolve
        them. *)

    aut : Aut.t;

    seeds: SeedSet.t;

    seed_nodes : NodeSet.t;

    dirty: bool;

    solver : Solver.t;

    iteration : int
  }

  let type_of (_, (ty, _)) = ty

  let stripped_trs abs_typed_trs =
    AbsTypedTrs.fold (
      fun (abs_lhs, abs_rhs) stripped_trs ->
        let lhs = AbsTypedPattern.strip abs_lhs in
        let rhs = AbsTypedPattern.strip abs_rhs in
        StrippedTrs.add (lhs, rhs) stripped_trs
    ) abs_typed_trs StrippedTrs.empty

  let known_state_opt t = function
    | Node.Known (_, q) -> Some q
    | _ -> None

  let known_state t node =
    match known_state_opt t node with
    | Some q -> q
    | None -> raise Not_found

  (* let is_known t node =
     match known_state_opt t node with
     | Some _ -> true
     | None -> false *)

  (* Add a constant value in the solver for the given state. *)
  (* let register_state q t =
     begin match TypeAut.StateMap.find_opt q t.solver_consts with
      | Some _ -> t
      | None ->
        let solver, c = Solver.new_const t.solver in
        {
          t with
          solver = solver;
          solver_consts = TypeAut.StateMap.add q c t.solver_consts
        }
     end *)

  (* Register a new node in the SMT-solver. *)
  (* It will be represented by a constant if it is known. *)
  (* let register_node node t =
     begin match node with
      | Node.Known (_, q) -> register_state q t
      | Node.Unknown _ -> t
     end *)

  exception Found of TypeAut.State.t

  (* For a given symbol and a list of known sub-types for this symbol,
     gives the actual type of the corresponding configuration among the given type-set [types]. *)
  let refined_type_of t partition (f, l) : TypeAut.State.t =
    let l = List.map (known_state t) l in
    try
      RegularTypePartition.iter (
        function regular_ty ->
          TypeAut.StateSet.iter (
            function ty ->
              TypeAut.LabeledConfigurationSet.iter (
                function
                | TypeAut.Configuration.Cons (f', l'), _ when TypeAut.Sym.equal f f' ->
                  if List.for_all2 TypeAut.State.equal l (List.map TypeAut.Configuration.normalized l')
                  then
                    raise (Found ty)
                | _ -> ()
              ) (TypeAut.configurations_for_state ty (TypeSystem.refined_knowledge_automaton t.type_system))
          ) regular_ty
      ) partition;
      raise Not_found
    with
    | Found q -> q

  let instance_of abs_ty q =
    match abs_ty with
    | Regular.Known types, _ ->
      RegularTypePartition.exists (TypeAut.StateSet.exists (Type.equal q)) types
    | Regular.Unknown, _ ->
      false

  (** Checks if [node] is an instance of [q]. *)
  let is_instance_of t q node =
    let module TypeAut2Aut = Automaton.Ext (TypeAut) (Aut) in
    Option.is_none (TypeAut2Aut.state_included_in (TypeSystem.refined_knowledge_automaton t.type_system) t.aut q node)

  exception Found_refinment of TypeAut.StateSet.t * TypeAut.State.t

  let refined_type_of_node t partition node =
    begin try
        RegularTypePartition.iter (
          function regular_ty ->
            TypeAut.StateSet.iter (
              function q ->
                if is_instance_of t q node then raise (Found_refinment (regular_ty, q))
            ) regular_ty
        ) partition;
        None
      with
      | Found_refinment (regular_ty, q) -> Some (regular_ty, q)
    end

  exception Found_instance of TypeAut.State.t

  let node_match t node q =
    match node with
    | Node.Known (_, q') ->
      (* TODO here q' could also be a subset of q... *)
      let do_match = TypeSystem.is_subtype t.type_system q q' in
      (* let do_match = TypeAut.State.equal q' q in *)
      (* if not do_match then debug "no match %t %t@." (Aut.State.print q) (Node.print node); *)
      do_match
    | Node.Unknown ((Regular.Unknown, _), _) ->
      let do_match = TypeAutExt.states_intersects q node (TypeSystem.refined_knowledge_automaton t.type_system) t.aut in
      (* if not do_match then debug "no match %t %t@." (Aut.State.print q) (Node.print node); *)
      do_match
    | Node.Unknown ((Regular.Known _, _), _) ->
      (* debug "no match %t %t@." (Aut.State.print q) (Node.print node); *)
      false

  let add_configuration_constraints (conf, node) t =
    let solver = match conf, node with
      | Aut.Configuration.Var (Node.Unknown (_, i)), Node.Unknown (_, j) ->
        let clause = (Solver.Eq (Solver.Variable i, Solver.Variable j)) in
        Solver.add clause t.solver
      | _ -> t.solver
    in
    { t with solver = solver }

  let comparable node node' =
    match Node.abs_ty node, Node.abs_ty node' with
    | (Regular.Known _, _), (Regular.Unknown, _) -> false
    | (Regular.Unknown, _), (Regular.Known _, _) -> false
    | _ -> true

  let add_configurations_pair_constraints (conf, node) (conf', node') t =
    if (Node.equal node node' && Aut.Configuration.equal conf conf') || not (comparable node node') then
      t
    else begin
      let smt_value = function
        | Node.Known _ -> failwith "Tried to get a SMT-solver variable for a known node."
        | Node.Unknown (_, id) ->
          Solver.Variable id
      in
      let rec are_unknown_nodes = function
        | [] -> true
        | (Aut.Configuration.Var (Node.Unknown _))::l -> are_unknown_nodes l
        | _ -> false
      in
      let solver = begin match conf, conf' with
        | Aut.Configuration.Cons (f, l), Aut.Configuration.Cons (f', l') when Aut.Sym.equal f f' && are_unknown_nodes l && are_unknown_nodes l' ->
          (* if Node.same_abstraction node node' then *)
          let l = List.map Aut.Configuration.normalized l in
          (* This function will be used to create a disjunction of disequality clauses later. *)
          let make_neq_disjunction n n' disjunction =
            match Node.try_compare n n' with
            | Some i when i != 0 -> disjunction
            | _ ->
              let clause = (Solver.Neq (smt_value n, smt_value n')) in
              clause::disjunction
          in
          begin match Node.try_compare node node' with
            | Some 0 ->
              t.solver
            | Some _ ->
              (* Log.debug (fun m -> m "CONJUCTION DIF FROM %t -> %t and %t -> %t\n@." (Aut.Configuration.print conf) (Node.print node) (Aut.Configuration.print conf') (Node.print node')); *)
              (* Nodes are differents for sure. *)
              (* Then configurations [conf] and [conf'] must be differents for
                 the resulting automaton to be deterministic. *)
              (* We first reduce the sub-nodes... *)
              let l' = List.map Aut.Configuration.normalized l' in
              (* ...and then create the disequalities disjunction. *)
              let disjunction = List.fold_right2 make_neq_disjunction l l' [] in
              let solver = Solver.add (Solver.Or disjunction) t.solver in
              (* Log.debug (fun m -> m "solver:\n%t" (Solver.print solver)); *)
              solver
            | None ->
              (* Log.debug (fun m -> m "CONJUCTION FROM %t -> %t and %t -> %t\n@." (Aut.Configuration.print conf) (Node.print node) (Aut.Configuration.print conf') (Node.print node')); *)
              (* We don't know yet if the nodes are equals. *)
              (* Then configurations [conf] and [conf'] must be differents if [node] and [node'] are. *)
              (* We first reduce the sub-nodes... *)
              let l' = List.map Aut.Configuration.normalized l' in
              (* ...and then create the disequalities disjunction. *)
              let disjunction = List.fold_right2 make_neq_disjunction l l' [] in
              (* To that, we add that the two nodes can be equals. *)
              let disjunction = (Solver.Eq (smt_value node, smt_value node'))::disjunction in
              (* Log.debug (fun m -> m "\nDIFFERENCIATE %t and %t\n@." (Node.print node) (Node.print node')); *)
              let solver = Solver.add (Solver.Or disjunction) t.solver in
              (* Log.debug (fun m -> m "solver:\n%t" (Solver.print solver)); *)
              solver
          end
        (* else t.solver *)
        | _ -> t.solver
      end
      in
      { t with solver = solver }
    end

  let is_reduced = function
    | Node.Unknown ((Regular.Known _, _), _) -> false
    | _ -> true

  let rec is_reduced_conf = function
    | Aut.Configuration.Var node -> is_reduced node
    | Aut.Configuration.Cons (_, l) ->
      List.for_all is_reduced_conf l

  let is_actual_seed t seed =
    SeedSet.exists (function seed' -> Seed.compare seed seed' = 0) t.seeds

  (** Register a new transition added to the completed automaton.
      This will add the transition node to the new_seed_nodes if it is a seed node. *)
  let register_transition (conf, (), node) t =
    (* Log.debug (fun m -> m "REGISTER TRANSITION: %t -> %t@." (Aut.Configuration.print conf) (Node.print node)); *)
    (* Check if it is a seed node. *)
    let t = match node, Seed.of_transition conf node with
      | Node.Unknown _, Some seed ->
        if is_actual_seed t seed then begin
          (* debug "register new seed node %t\n@." (Node.print node); *)
          (* If it is, add it to the new_seed_nodes. *)
          { t with seed_nodes = NodeSet.add node t.seed_nodes }
        end else
          t
      | _ -> t
    in
    let t = { t with dirty = true } in
    if is_reduced node && is_reduced_conf conf then
      let t = add_configuration_constraints (conf, node) t in
      Aut.fold_transitions (
        fun conf' () node' t ->
          if is_reduced node' && is_reduced_conf conf' then
            add_configurations_pair_constraints (conf, node) (conf', node') t
          else
            t
      ) t.aut t
    else
      t

  (** Add a new transition in the completed automaton.
      It must be normalized. *)
  let add_transition (conf, (), node) t =
    let t = { t with aut = Aut.add_transition conf () node t.aut } in
    register_transition (conf, (), node) t

  let add_configuration conf abs_ty t =
    let nodes = Aut.states_for_configuration conf t.aut in
    let node_opt = Aut.LabeledStateSet.search_opt (
        function
        | Node.Known (abs_ty', _), _ when AbsType.equal abs_ty abs_ty' (* instance_of abs_ty q *) ->
          true
        | Node.Unknown (abs_ty', _), _ when AbsType.equal abs_ty abs_ty' ->
          (* Log.debug (fun m -> m "unknown node matching"); *)
          true
        | _, _ -> false
      ) nodes
    in
    match node_opt with
    | Some (node, _) ->
      (* Log.debug (fun m -> m "%t already maches %t" (AbsType.print abs_ty) (Node.print node)); *)
      node, t
    | None ->
      let node, t = match conf, abs_ty with
        | Aut.Configuration.Cons (f, l), (Regular.Known partition, _) ->
          let l = List.map (Aut.Configuration.normalized) l in
          begin try
              let q = refined_type_of t partition (f, l) in
              Node.Known (abs_ty, q), t
            with
            | Not_found ->
              Node.create_from_abstract abs_ty, t
          end
        | _, _ ->
          (* let l = List.map (Aut.Configuration.normalized) l in *)
          Node.create_from_abstract abs_ty, t
      in
      (* Log.debug (fun m -> m "Add conf %t to %t" (Aut.Configuration.print conf) (Node.print node)); *)
      let t = add_transition (conf, (), node) t in
      node, { t with dirty = true }

  let abs_type_check t abs_type node =
    match abs_type, node with
    | (Regular.Unknown, q), Node.Unknown ((Regular.Unknown, q'), _) ->
      TypeSystem.is_subtype t.type_system q q'
    (* | (Regular.Known _, q), Node.Unknown ((Regular.Unknown, q'), _) ->
       TypeSystem.is_subtype t.type_system q q' *)
    | (Regular.Known partition, q), Node.Unknown ((Regular.Known partition', q'), _) ->
      if Type.equal q q' then
        RegularTypePartition.equal partition partition'
        (* else if TypeSystem.is_subtype t.type_system q q' then (* FIXME not sure here *)
           failwith "TODO : subtype partition while matching." *)
      else
        false
    | (Regular.Known partition, q), Node.Known ((Regular.Known partition', q'), _) ->
      if Type.equal q q' then
        RegularTypePartition.equal partition partition'
        (* else if TypeSystem.is_subtype t.type_system q q' then (* FIXME not sure here *)
           failwith "TODO : subtype partition while matching." *)
      else
        false
    | _, _ ->
      false

  let rec abs_match ?(visited=NodeSet.empty) t pattern node =
    (* Log.debug (fun m -> m "matching %t to %t" (AbsTypedPattern.print pattern) (Node.print node)); *)
    if NodeSet.mem node visited then None else begin
      let exception Found of Node.t AbsTypedPattern.Substitution.t in
      if abs_type_check t (type_of pattern) node then
        begin
          (* Log.debug (fun m -> m "ok"); *)
          match pattern, node with
          (* | _, Node.Known _ ->
             failwith "TODO handle known nodes" *)
          | (AbsTypedPattern.Cons (f, l), _), node ->
            begin try
                let confs = Aut.configurations_for_state node t.aut in
                Aut.LabeledConfigurationSet.iter (
                  function
                  | Aut.Configuration.Cons (f', l'), _ when Aut.Sym.equal f f' ->
                    begin
                      match List.map2_opt (abs_match t) l (List.map Aut.Configuration.normalized l') with
                      | Some sigmas ->
                        let linear _ _ = raise (Invalid_argument "pattern must me linear") in
                        let sigma = List.fold_right (AbsTypedPattern.Substitution.union linear) sigmas AbsTypedPattern.Substitution.empty in
                        raise (Found sigma)
                      | None -> ()
                    end
                  | Aut.Configuration.Var node', _ ->
                    begin match abs_match ~visited:(NodeSet.add node visited) t pattern node' with
                      | Some sigma -> raise (Found sigma)
                      | None -> ()
                    end
                  | _ -> ()
                ) confs;
                None
              with
              | Found sigma -> Some sigma
            end
          | (AbsTypedPattern.Var x, _), node ->
            Some (AbsTypedPattern.Substitution.singleton x node)
          | (AbsTypedPattern.Cast _, _), _ ->
            raise (Invalid_argument "Pattern cast not handled in left-hand-sides")
        end
      else
        begin
          Log.debug (fun m -> m "Type missmatch");
          None
        end
    end

  (** Fold the new critical pairs. *)
  let fold_rec_critical_pairs t f x =
    NodeSet.fold (
      fun node x ->
        (* Log.debug (fun m -> m "considering node %t" (Node.print node)); *)
        AbsTypedTrs.fold (
          fun (lhs, rhs) x ->
            match abs_match t lhs node with
            | Some sigma -> f (lhs, rhs) node sigma x
            | None -> x
        ) t.abs_typed_trs x
    ) t.seed_nodes x

  (* Connect node [a] to node [b] so to have [a] -> [b]. *)
  let connect a b t =
    begin
      match Node.abs_ty a, Node.abs_ty b with
      | (Regular.Known _, _), (Regular.Unknown, _) ->
        Log.err (fun m -> m "Invalid node connection %t -> %t" (Node.print a) (Node.print b));
        failwith "Invalid node connection"
      | _ -> ()
    end;
    if Aut.mem (Aut.Configuration.of_var a) () b t.aut then
      t
    else begin
      add_transition (Aut.Configuration.of_var a, (), b) t
      (* {
         t with
         aut = Aut.add_transition (Aut.Configuration.of_var a) () b t.aut;
         dirty = true
         } *)
    end

  (* Add the given abs-typed pattern to the completion automaton.
     New states are created only if no suitable existing state is found. *)
  let rec add_abs_pattern t sigma = function
    | AbsTypedPattern.Cons (f, l), (abs_ty, _) ->
      (* Add the subpatterns to the automaton and return the list of recognizing states. *)
      let l', t = List.fold_right (
          fun sub_pattern (l', t) ->
            let node, t = add_abs_pattern t sigma sub_pattern in
            node::l', t
        ) l ([], t)
      in
      (* The configuration we want to add in the completed automaton. *)
      let conf = Aut.Configuration.Cons (f, List.map Aut.Configuration.of_var l') in
      (* Add it to the automaton (and create a new state if necessary). *)
      add_configuration conf abs_ty t
    | AbsTypedPattern.Var x, ((Regular.Known partition, simple_ty), _) ->
      begin match refined_type_of_node t partition (sigma x) with
        | Some (_, q) -> Node.Known ((Regular.Known partition, simple_ty), q), t
        | None -> sigma x, t
      end
    | AbsTypedPattern.Var x, _ ->
      sigma x, t
    | AbsTypedPattern.Cast pattern, (abs_ty, _) ->
      let node, t = add_abs_pattern t sigma pattern in
      let conf = Aut.Configuration.Var node in
      add_configuration conf abs_ty t

  (* Reduce unknown known-partition nodes into known known-partition nodes.
     New SMT constraints are generated to account for the new configurations. *)
  let reduce t =
    Log.info (fun m -> m "Reduction...");
    (* Step 1. Reduce the every unreduced nodes. *)
    let module Hashtbl = Hashtbl.Make (Node) in
    let table = Hashtbl.create 8 in
    let rec reduce_node node =
      match node with
      | Node.Unknown ((Regular.Known partition, simple_ty), _) ->
        let abs_ty = Regular.Known partition, simple_ty in
        begin
          match Hashtbl.find_opt table node with
          | Some reduced_node -> reduced_node
          | None ->
            begin try
                let confs = Aut.configurations_for_state node t.aut in
                Aut.LabeledConfigurationSet.iter (
                  function
                  | Aut.Configuration.Var node', _ ->
                    let node' = reduce_node node' in
                    RegularTypePartition.iter (
                      function regular_ty ->
                        TypeAut.StateSet.iter (
                          function q ->
                            if node_match t node' q then
                              raise (Found_instance q)
                            else ()
                        ) regular_ty
                    ) partition
                  | Aut.Configuration.Cons (f, l), _ ->
                    let l = List.map (Aut.Configuration.normalized >> reduce_node) l in
                    let as_refined_ty node =
                      match node with
                      | Node.Known (_, q) -> Some q
                      | _ ->
                        (* Aut.fold_epsilon_class (
                           fun node q_opt ->
                            match node, q_opt with
                            | _, Some q -> Some q
                            | Node.Known (_, q), None -> Some q
                            | _ -> None
                           ) node t.aut None *)
                        None
                    in
                    begin match List.map_opt as_refined_ty l with
                      | Some l ->
                        let conf = TypeAut.Configuration.Cons (f, List.map TypeAut.Configuration.of_var l) in
                        let states = TypeAut.states_for_configuration conf (TypeSystem.refined_knowledge_automaton t.type_system) in
                        begin match TypeAut.LabeledStateSet.search_opt (function (q, _) -> instance_of abs_ty q) states with
                          | Some (q, _) -> raise (Found_instance q)
                          | None -> ()
                        end
                      | None -> ()
                    end
                ) confs;
                Log.err (fun m -> m "Cannot reduce node %t in\n%t" (Node.print node) (Aut.print t.aut));
                let sample = Aut.pick_term_in node t.aut in
                Log.err (fun m -> m "Sample:\n%t" (Aut.BoundTerm.print sample));
                (* raise (Invalid_argument "unable to reduce node") *)
                raise (IrreducibleNonValue (Aut.BoundTerm.strip sample))
              with
              | Found_instance q ->
                let reduced_node = Node.Known ((Regular.Known partition, simple_ty), q) in
                Hashtbl.add table node reduced_node;
                reduced_node
            end
        end
      | _ ->
        (* already reduced *)
        node
    in
    let rec reduce_conf = function
      | Aut.Configuration.Var node ->
        Aut.Configuration.Var (reduce_node node)
      | Aut.Configuration.Cons (f, l) ->
        Aut.Configuration.Cons (f, List.map reduce_conf l)
    in
    (* Step 2. Collect unreduced transitions. *)
    let unreduced_transitions = Aut.fold_transitions (
        fun conf () node unreduced_transitions ->
          if not (is_reduced node) || not (is_reduced_conf conf) then
            (conf, node)::unreduced_transitions
          else
            unreduced_transitions
      ) t.aut []
    in
    (* Step 3. Reduce unreduced nodes. *)
    let map_label () = () in
    let reduced_aut = Aut.map map_label reduce_node t.aut in
    let reduced_seed_nodes = NodeSet.map reduce_node t.seed_nodes in
    (* Step 4. Create new SMT constraints. *)
    let t = List.fold_right (
        fun (conf, node) t ->
          (* Log.debug (fun m -> m "Unreduced transition %t -> %t" (Aut.Configuration.print conf) (Node.print node)); *)
          let reduced_conf = reduce_conf conf in
          let reduced_node = reduce_node node in
          let t = add_configuration_constraints (reduced_conf, reduced_node) t in
          Aut.fold_transitions (
            fun conf' () node' t ->
              add_configurations_pair_constraints (reduced_conf, reduced_node) (conf', node') t
          ) reduced_aut t
      ) unreduced_transitions t
    in
    (* Final step. Return. *)
    {
      t with
      aut = reduced_aut;
      seed_nodes = reduced_seed_nodes
    }

  (** Perform a completion step. *)
  let step t =
    Log.debug (fun m -> m "Completion step");
    (* Log.debug (fun m -> m "COMPLETION STEP:\n%t\n@." (Aut.print t.aut)); *)
    (* Iterate through the new critical pairs. *)
    fold_rec_critical_pairs t (
      (* For each critical pair *)
      fun (lhs, rhs) node sigma t ->
        Log.debug (
          fun m -> m "Found critical pair: %t -> %t at node %t@."
              (AbsTypedPattern.print lhs)
              (AbsTypedPattern.print rhs)
              (Node.print node));
        (* if is_known t node then begin
           failwith "?"
           end; *)
        (* Setup the substitution function. *)
        let sigma x = AbsTypedPattern.Substitution.find x sigma in
        (* Find a node to recognize the right-hand side of the rule (or create it). *)
        let node', t = add_abs_pattern t sigma rhs in
        (* Connect the left-side node and the right-side node together. *)
        connect node' node t
    ) t

  (* Run the completion algorithm. *)
  let rec complete t =
    Log.info (fun m -> m "Completion...");
    if (not t.dirty) then
      begin
        let t = reduce t in
        (* debug "completion end:\n%t@." (AbsAut.print t.aut); *)
        t
      end
    else
      begin
        let t = step { t with dirty = false } in
        (* debug "step end: %t@." (AbsAut.print t.aut); *)
        complete t
      end

  let grow t =
    (* Log.debug (fun m -> m "growing@."); *)
    let rec aux aut = function
      | 0 -> if t.iteration > 0 then failwith "Unable to find new samples!" else aut, []
      | n ->
        let aut, new_transitions = Aut.grow aut in
        if new_transitions = [] then aux aut (n-1) else aut, new_transitions
    in
    let aut, new_transitions = aux t.aut 100 in
    let t = { t with aut = aut } in
    if new_transitions = [] && t.iteration > 0 then failwith "Unable to find new samples!";
    List.fold_right register_transition new_transitions t

  let solve t =
    Log.info (fun m -> m "Solving...");
    (* Log.debug (fun m -> m "solver:\n%t" (Solver.print t.solver)); *)
    (* debug "\nAUTOMATON after completion:\n%t@." (Aut.print t.aut); *)
    (* debug "Resolving...\n@."; *)
    match Solver.solve t.solver with
    | Solver.Sat model ->
      (* debug "MODEL %t\n@." (PPrint.print_map (module Solver.Model) model); *)
      let map_data _ = () in
      let map_label () = () in
      let map_state : Node.t -> SolvedAut.State.t = function
        | Node.Unknown ((Regular.Unknown, q), id) ->
          (* debug "MODEL FIND %d for node %t" id (Node.print (Node.Unknown ((Regular.Unknown, q), id))); *)
          let i = match Solver.Model.find_opt id model with
            | Some i -> i
            | None -> 0
          in
          Regular.New (Regular.Unknown, q, i)
        | Node.Unknown ((Regular.Known _, _), _) ->
          raise (Invalid_argument "Some node has an unreduced known partition. This mean either your input TRS is uncomplete, or this is a bug.")
        | Node.Known (_, q) ->
          Regular.Old q
      in
      (* Remove reflexive transitions and old transitions. *)
      let non_reflex conf _ q =
        match conf with
        | Aut.Configuration.Var q' -> not (Aut.State.equal q q')
        | Aut.Configuration.Cons (_, l) ->
          Node.is_unknown q || List.for_all Node.is_unknown (List.map Aut.Configuration.normalized l)
      in
      let solved_aut = MapAut.map ~filter:non_reflex map_data map_label map_state t.aut in
      Log.debug (fun m -> m "Proposed solution:\n%t@." (SolvedAut.print solved_aut));
      Solver.Sat solved_aut
    | Solver.Unsat ->
      (* Solver.solve ~debug:true solver; *)
      Solver.Unsat
    | Solver.Unknown ->
      (* Solver.solve ~debug:true solver; *)
      Solver.Unknown

  let rec is_newtyped_pattern = function
    | Matching.TypedPattern.Cons (_, l), (Some (Regular.New _), _) ->
      List.for_all is_newtyped_pattern l
    | Matching.TypedPattern.Var _, (Some (Regular.New _), _) -> true
    | _ -> false

  let is_unsolved_seed = function
    | Matching.TypedPattern.Cons (_, l), _ ->
      List.for_all is_newtyped_pattern l
    | Matching.TypedPattern.Var _, (Some (Regular.Old _), _) -> true
    | _ -> false

  (* Fold all the critical pairs in a solved automaton. *)
  let fold_all_critical_pairs t f solved_aut x =
    let matching = Matching.create true t.trs_patterns solved_aut in
    (* Log.debug (fun m -> m "Matched@."); *)
    (* debug "MATCHING:\n%t@." (Matching.print matching); *)
    Matching.fold (
      fun q lhs m x ->
        (* Log.debug (fun p -> p "MATCH %t: %t" (SolvedAut.State.print q) (Matching.Match.print m)); *)
        if is_unsolved_seed (Matching.Match.pattern m) then begin
          let rhss = StrippedTrs.rights_of lhs t.trs in
          List.fold_right (f q m lhs) rhss x
        end else begin
          (* Log.debug (fun p -> p "(resolved)"); *)
          x
        end
    ) matching x

  (* let rec decorate sigma state : Pattern.t -> SolvedAutPatterns.bound_pattern = function
     | Pattern.Cons (f, l) -> Matching.TypedPattern.Cons (f, List.map (decorate sigma None) l), (state, None)
     | Pattern.Var x -> Matching.TypedPattern.Var x, (Some (sigma x), None) *)

  (** Type the given term with newstates in the solved automaton. *)
  let rec type_term solved_aut ty term =
    let module Term = Term.Make (SolvedAut.Sym) in
    (* Log.debug (fun m -> m "Typing %t with %t\n" (Term.print term) (Matching.TypedTerm.Type.print ty)); *)
    begin match term with
      | Term.Term (f, l) ->
        let l = List.map (type_term solved_aut (None, None)) l in
        let sub_confs = List.map
            (
              function
              | _, (Some ty, _) -> SolvedAut.Configuration.Var ty
              | _ -> failwith "untyped subterm" (* cannot happen. *)
            ) l
        in
        let conf = SolvedAut.Configuration.Cons (f, sub_confs) in
        let states = SolvedAut.states_for_configuration conf solved_aut in
        let q, label = SolvedAut.LabeledStateSet.search (function Regular.New _, _ -> true | _ -> false) states in
        begin match Matching.TypedTerm.Type.product (Some q, Some label) ty with
          | Some ty -> Matching.TypedTerm.Term (f, l), ty
          | None ->
            Log.err (fun m -> m "SOLVED AUT:\n%t@." (SolvedAut.print solved_aut));
            Log.err (fun m -> m "ERROR while typing %t with %t (given) and %t (found)\n"
                        (Term.print term)
                        (Matching.TypedTerm.Type.print ty)
                        (Matching.TypedTerm.Type.print (Some q, Some label)));
            raise (Invalid_argument "Incompatible types")
        end
    end

  (** Checks if L(q') is included in L(q) *)
  (* let included_in solved_aut q q' =
     let module SolvedAut2 = Automaton.Ext (SolvedAut) (SolvedAut) in
     SolvedAut2.state_included_in solved_aut solved_aut q q' *)
  (* SolvedAut.recognizes_state_in q q' solved_aut *)

  let rec fold_recognized_pattern_matches_in solved_aut (g : Matching.TypedPattern.t -> 'a -> 'a) pattern q (accu:'a) : 'a =
    begin match pattern with
      | Pattern.Cons (f, l) ->
        let confs : SolvedAut.LabeledConfigurationSet.t = SolvedAut.configurations_for_state q solved_aut in
        SolvedAut.LabeledConfigurationSet.fold (
          fun conf accu ->
            match conf with
            | SolvedAut.Configuration.Cons (f', l'), label when SolvedAut.Sym.equal f f' ->
              let l' = List.map SolvedAut.Configuration.normalized l' in
              List.inner_fold2 (
                fun l (accu:'a) ->
                  let pattern = Matching.TypedPattern.Cons (f, l), (Some q, Some label) in
                  g pattern accu
              ) (fold_recognized_pattern_matches_in solved_aut) l l' accu
            | _ -> accu
        ) confs accu
      | Pattern.Var x ->
        let pattern = Matching.TypedPattern.Var x, (Some q, None) in
        g pattern accu
    end

  let fold_recognized_pattern_matches_in_with_substitutions solved_aut _type_of g pattern q accu =
    fold_recognized_pattern_matches_in solved_aut (
      fun matching_pattern accu ->
        let sigma = Matching.TypedPattern.fold (
            fun matching_pattern (sigma : SolvedAut.StateSet.t Matching.TypedPattern.Substitution.t) ->
              match matching_pattern with
              | Matching.TypedPattern.Var x, (Some ty, _) ->
                begin match Matching.TypedPattern.Substitution.find_opt x sigma with
                  | Some types -> Matching.TypedPattern.Substitution.add x (SolvedAut.StateSet.add ty types) sigma
                  | None -> Matching.TypedPattern.Substitution.add x (SolvedAut.StateSet.singleton ty) sigma
                end
              | _ -> sigma
          ) matching_pattern Matching.TypedPattern.Substitution.empty
        in
        g matching_pattern sigma accu
    ) pattern q accu

  let recognized_pattern_in solved_aut solved_aut_lite type_of_var pattern q =
    let exception Found of Matching.TypedPattern.t * (Matching.TypedPattern.Substitution.key -> SolvedAut.BoundTerm.t) in
    let _ = 0 in (* TODO just for coloration. *)
    try fold_recognized_pattern_matches_in_with_substitutions solved_aut type_of_var (
        fun matching_pattern sigma () ->
          try
            let sigma = Matching.TypedPattern.Substitution.mapi (
                fun x types ->
                  let states = SolvedAut.StateSet.add (type_of_var x) types in
                  let new_states = SolvedAut.StateSet.filter (function Regular.New _ -> true | _ -> false) states in
                  (* New-states have empty intersection by construction.
                     So we can raise a Not_found exception if we have more that one new-state in [states]. *)
                  if SolvedAut.StateSet.cardinal new_states > 1 then raise Not_found;
                  let term = SolvedAut.pick_term_in_intersection states solved_aut_lite in

                  (* Log.debug (fun m ->
                      let module Term = Term.Make (SolvedAut.Sym) in
                      m "Found term in intersection: %t : %t@." (Term.print term) (SolvedAut.StateSet.print SolvedAut.State.print "," states);
                      SolvedAut.StateSet.iter (
                        function q ->
                          begin
                            (* m "trying with %t@." (SolvedAut.State.print q); *)
                            match SolvedAut.type_term_in q term solved_aut with
                            | Some bterm -> m "=> %t : %t@." (SolvedAut.BoundTerm.print bterm) (SolvedAut.State.print q)
                            | None -> failwith "unable to retype found term"
                          end
                      ) states;
                     );

                     term *)
                  match SolvedAut.type_term_in (type_of_var x) term solved_aut with
                  | Some sample ->
                    (* Log.debug (fun m -> m "FFOUND!"); *)
                    sample
                  | None -> failwith "Picked term is wrong."
              ) sigma
            in
            let sigma x =
              begin match Matching.TypedPattern.Substitution.find_opt x sigma with
                | Some sample -> sample
                | None ->
                  let q = type_of_var x in
                  let sample = SolvedAut.pick_term_in ~epsilon:false q solved_aut_lite in
                  (* SolvedAut.BoundTerm.strip sample *)
                  sample
              end
            in
            raise (Found (matching_pattern, sigma))
          with
          | Not_found ->
            ()
      ) pattern q ();
      None
    with
    | Found (matching_pattern, sigma) -> Some (matching_pattern, sigma)

  (** Try to find an instance of the given [pattern] that is NOT recognized by
      the given state [q].*)
  let unrecognized_pattern_in t solved_aut solved_aut_lite type_of_var (q: SolvedAut.State.t) pattern =
    (* Log.debug (fun m -> m "check %t in %t" (Pattern.print pattern) (SolvedAut.State.print q)); *)
    (* Pattern.fold (fun x () ->
        let module Term = Term.Make (SolvedAut.Sym) in
        Log.debug (fun m -> m "query %t <- %t" (Pattern.Var.print x) (SolvedAut.State.print (type_of_var x)))
       ) pattern (); *)
    let exception Found of (Matching.TypedPattern.Substitution.key -> SolvedAut.BoundTerm.t) in
    begin match q with
      | Regular.Old regular_ty ->
        let complements = TypeSystem.complements_of t.type_system regular_ty in
        (* if TypeSystem.TypeSetSet.is_empty complements then
           debug "NO COMPLEMENT for %t" (Type.print regular_ty); *)
        begin try
            TypeSystem.TypeSetSet.iter (
              function complement ->
                TypeSystem.TypeSet.iter (
                  function q ->
                    begin match recognized_pattern_in solved_aut solved_aut_lite type_of_var pattern (Regular.Old q) with
                      | Some (matching_pattern, sigma) ->
                        (* (Matching.TypedPattern.instanciate (fun x _ -> type_term solved_aut (None, None) (sigma x)) matching_pattern) *)
                        (* Log.debug (fun m -> m "Found pattern in complement: %t\n@." (Matching.TypedPattern.print matching_pattern)); *)
                        raise (Found sigma)
                      | None -> ()
                    end
                ) complement
            ) complements;
            None
          with
          | Found sigma -> Some sigma
        end
      | Regular.New _ ->
        (* Check exact recognition. *)
        begin match recognized_pattern_in solved_aut solved_aut_lite type_of_var pattern q with
          | Some _ ->
            None
          | None ->
            let sigma x =
              let q = type_of_var x in
              let sample = SolvedAut.pick_term_in ~epsilon:false q solved_aut_lite in
              (* SolvedAut.BoundTerm.strip sample *)
              sample
            in
            Some sigma
        end
    end

  let unsolved_critical_pair_sample t solved_aut solved_aut_lite =
    let exception Found of SolvedAut.BoundTerm.t in
    let _ = 0 in
    (* Fold the critical pairs to check that they are all solved. *)
    begin try fold_all_critical_pairs t (
        fun q m lhs rhs () ->
          let typed_lhs = Matching.Match.pattern m in
          let ty = match typed_lhs with
            | _, (Some ty, _) -> ty
            | _ -> failwith "Untyped matching"
          in
          if SolvedAut.State.equal ty q then begin
            let sub = Matching.TypedPattern.substitution typed_lhs in
            let type_of_var x = match Matching.TypedPattern.Substitution.find x sub with
              | (Some q, _) -> q
              | _ -> failwith "Matching should type variables"
            in
            (* let typed_rhs = decorate type_of_var None rhs in
               let confs = SolvedAut.configurations_for_state q solved_aut in *)
            (* Log.debug (fun m -> m "Checking critical pair %t -> %t %t@."
                          (Matching.TypedPattern.print typed_lhs)
                          (Pattern.print rhs)
                          (SolvedAut.State.print q)); *)
            (* Log.debug (fun m -> m "critical pair@."); *)
            begin match unrecognized_pattern_in t solved_aut solved_aut_lite type_of_var q rhs with
              | None ->
                (* Log.debug (fun m -> m "solved@."); *)
                (* debug "checked critical pair %t -> %t %t@."
                   (Matching.TypedPattern.print typed_lhs)
                   (Matching.TypedPattern.print typed_rhs)
                   (SolvedAut.State.print q); *)
                ()

              (* let e = e || SolvedAut.LabeledConfigurationSet.exists (
                  function
                  | SolvedAut.Configuration.Cons _, _ -> false
                  | SolvedAut.Configuration.Var q', _ ->
                    (* SolvedAutPatterns.recognizes_bound_pattern_in ~epsilon:false q' typed_rhs solved_aut *)
                    recognize_pattern_in solved_aut sigma q' rhs
                 ) confs
                 in
                 if not e then begin
                 debug "UNSOLVED critical pair %t -> %t %t@."
                  (Matching.TypedPattern.print typed_lhs)
                  (Matching.TypedPattern.print typed_rhs)
                  (SolvedAut.State.print q);
                 begin match SolvedAutPatterns.instanciate_pattern_opt typed_lhs solved_aut with
                  | Some typed_lhs_sample ->
                    raise (Found typed_lhs_sample)
                  | None -> failwith "Uninstanciable rule"
                 end *)

              | Some sigma ->
                (* Log.debug (fun m -> m "unsolved@."); *)
                (* let pattern_sample = Matching.TypedPattern.of_term sample in *)

                (* Log.debug (fun m -> m "Unsolved critical pair %t\n%t -> %t %t"
                              (Matching.TypedPattern.print typed_lhs)
                              (Matching.Pattern.print lhs)
                              (Matching.Pattern.print rhs)
                              (SolvedAut.State.print q)); *)
                (* Matching.TypedPattern.iter (function
                    | Matching.TypedPattern.Var x, ty ->
                      let module Term = Term.Make (SolvedAut.Sym) in
                      Log.debug (fun m -> m "%t <- %t" (Matching.Pattern.Var.print x) (SolvedAut.BoundTerm.print (sigma x)))
                    | _ -> ()
                   ) typed_lhs;
                *)
                (* let sample_sigma = Matching.TypedPattern.get_substitution typed_rhs sample in *)
                (* debug "found substitution\n@."; *)
                let lhs_sample = Matching.TypedPattern.instanciate (fun x _ty ->
                    (* Log.debug (fun m -> m "typing var %t" (Matching.Pattern.Var.print x)); *)
                    (* type_term solved_aut ty (sigma x) *)
                    sigma x
                  ) typed_lhs
                in
                (* Log.debug (fun m -> m "sample@."); *)
                (* Log.debug (fun m -> m "LHS sample: %t\n@." (SolvedAut.BoundTerm.print lhs_sample)); *)
                raise (Found lhs_sample)
            end
          end
      ) solved_aut ();
        None
      with
      | Found sample -> Some sample
    end

  (** Find missing configuration in missing solution automaton. *)
  let missing_conf t full_solved_aut solved_aut =
    (* Log.debug (fun m -> m "find missing confs in %t" (SolvedAut.print solved_aut)); *)
    let exception Found of SolvedAut.BoundTerm.t option in
    (* [table] stores for each type its abstraction (a set of new states). *)
    let module Hashtbl = Hashtbl.Make (Type) in
    let table = Hashtbl.create 8 in
    (* Populate the table. *)
    SolvedAut.fold_transitions (
      fun _conf () solved_q () ->
        (* debug "transition %t -> %t@." (SolvedAut.Configuration.print conf) (SolvedState.print solved_q); *)
        match solved_q with
        | Regular.New (Regular.Unknown, simple_ty, _id) ->
          (* debug "new state %t %d@." (Aut.State.print q) id; *)
          let states = match Hashtbl.find_opt table simple_ty with
            | Some states ->
              SolvedAut.StateSet.add solved_q states
            | None ->
              SolvedAut.StateSet.singleton solved_q
          in
          Hashtbl.replace table simple_ty states
        | _ -> ()
    ) solved_aut ();
    (* Log.debug (fun m -> m "separation:";
                Hashtbl.iter (
                  fun q states ->
                    Log.debug (fun m -> m "%t => %t"
                                  (TypeAut.State.print q)
                                  (SolvedAut.StateSet.print SolvedAut.State.print ", " states))
                ) table); *)
    try
      let check_configuration f sub_states (abs_partition, simple_ty) =
        (* Log.debug (fun m -> m "checking configuration: %t(%t):%t"
                      (TypeAut.Sym.print f)
                      (List.print TypeAut.State.print ", " sub_states)
                      (AbsType.print (abs_partition, simple_ty))); *)
        let sub_combinations = try
            List.map (Hashtbl.find table) sub_states
          with Not_found -> (* a state doesn't even have an abstraction. *)
            raise (Found None)
        in
        SolvedAut.StateSet.fold_inline_combinations (
          fun sub_nodes () ->
            let l' = List.map SolvedAut.Configuration.of_var sub_nodes in
            let solved_conf = SolvedAut.Configuration.Cons (f, l') in
            let same_ty_node (conf, _) =
              match conf, abs_partition with
              | Regular.New (_, simple_ty', _), Regular.Unknown ->
                TypeSystem.TypeAut.State.equal simple_ty simple_ty'
              | Regular.Old _, Regular.Unknown -> (* FIXME... *)
                true
              | Regular.Old ty, Regular.Known partition ->
                RegularTypePartition.exists (TypeAut.StateSet.mem ty) partition
              | _ -> false
            in
            let nodes = SolvedAut.states_for_configuration solved_conf solved_aut in
            if SolvedAut.LabeledStateSet.exists same_ty_node nodes then () else begin
              let sub_samples = List.map (fun node -> SolvedAut.pick_term_in ~epsilon:false node full_solved_aut) sub_nodes in
              (* Log.debug (fun m -> m "Missing configuration: %t:%t" (SolvedAut.Configuration.print solved_conf) (AbsType.print (abs_partition, simple_ty))); *)
              let new_state = Regular.New (abs_partition, simple_ty, Node.next_id ()) in
              let sample = SolvedAut.BoundTerm.Term (f, sub_samples), (Some new_state, None) in
              raise (Found (Some sample))
            end
        ) sub_combinations ()
      in
      Hashtbl.iter (
        fun simple_ty _ ->
          let confs = TypeAut.configurations_for_state simple_ty (TypeSystem.refined_automaton t.type_system) in
          TypeAut.LabeledConfigurationSet.iter (
            function (conf, _) ->
            match conf with
            | TypeAut.Configuration.Cons (f, l) ->
              let sub_states = List.map TypeAut.Configuration.normalized l in
              check_configuration f sub_states (Regular.Unknown, simple_ty)
            | _ ->
              raise (Invalid_argument "Type-system automaton must be epsilon-free")
          ) confs
      ) table;
      SeedSet.iter (
        function (f, sub_abs_tys, abs_ty) ->
          let sub_states = List.map snd sub_abs_tys in
          check_configuration f sub_states abs_ty
      ) t.seeds;
      (* Log.debug (fun m -> m "Complete@."); *)
      None
    with
    | Found sample_opt ->
      (* Log.debug (fun m -> m "Uncomplete...@."); *)
      Some sample_opt

  let rec add_sample (sample : SolvedAut.BoundTerm.t) t =
    let rec add_subterm term (t, subnodes) =
      match term with
      | (SolvedAut.BoundTerm.Term (f, l), (Some (Regular.New (_, ty, _)), _)) ->
        let (t, l) = List.fold_right add_subterm l (t, []) in
        let node, t = add_configuration (Aut.Configuration.Cons (f, List.map Aut.Configuration.of_var l)) (Regular.Unknown, ty) t in
        t, (node::subnodes)
      | (_, (None, _)) ->
        failwith "Unknown type for subterm"
      | _ -> failwith "Subterm is not new"
    in
    let matching_seed f solved_ty (f', _subs, abs_ty) =
      (* debug "matching %t\n" (Seed.print (f', subs, abs_ty)); *)
      Aut.Sym.equal f f' &&
      match abs_ty, solved_ty with
      | (Regular.Known partition, _), Regular.Old q ->
        RegularTypePartition.exists (TypeAut.StateSet.exists (function q' ->
            (* debug "comparing %t %t\n" (Type.print q) (Type.print q'); *)
            TypeAut.State.equal q q')) partition
      | (Regular.Known partition, simple_ty), Regular.New (Regular.Known partition', simple_ty', _) ->
        TypeAut.State.equal simple_ty simple_ty' && RegularTypePartition.equal partition partition'
      | (Regular.Unknown, simple_ty), Regular.New (Regular.Unknown, simple_ty', _) ->
        TypeAut.State.equal simple_ty simple_ty'
      | _ -> false
    in
    begin match sample with
      | SolvedAut.BoundTerm.Term (f, l), (Some solved_ty, _) ->
        begin match SeedSet.search_opt (matching_seed f solved_ty) t.seeds with
          | Some (_, _, abs_ty) ->
            if List.exists (function (_, (Some (Regular.New _), _)) -> true | _ -> false) l then begin
              (* Log.debug (fun m -> m "Sample %t : %t" (SolvedAut.BoundTerm.print sample) (SolvedAut.State.print solved_ty)); *)
              let t, l = List.fold_right add_subterm l (t, []) in
              (* Log.debug (fun m -> m "ADD SAMPLE"); *)
              let _, t = add_configuration (Aut.Configuration.Cons (f, List.map Aut.Configuration.of_var l)) abs_ty t in
              t
            end else
              List.fold_right add_sample l t (* only add subterms *)
          | _ -> (* It's just a value. *)
            begin match solved_ty with
              | Regular.New (abs_partition, simple_ty, _) ->
                let abs_ty = abs_partition, simple_ty in
                let t, l = List.fold_right add_subterm l (t, []) in
                let _, t = add_configuration (Aut.Configuration.Cons (f, List.map Aut.Configuration.of_var l)) abs_ty t in
                t
              | _ ->
                (* failwith "old sample should already be added" *)
                List.fold_right add_sample l t (* only add subterms *)
            end
            (* debug "NEEDED type %t:%t in seeds\n%t@." (Aut.Sym.print f) (SolvedAut.State.print solved_ty) (PPrint.print_ext_set (module SeedSet) Seed.print "\n" t.seeds); *)
            (* failwith "unable to get the partition of the given type refinement." *)
        end
      | _ -> failwith "Untyped term sample" (* should never happend. *)
    end

  let add_sample sample t =
    let t = add_sample sample t in
    if not t.dirty then begin
      let ts = TypeSystem.refined_knowledge_automaton t.type_system in
      Log.debug (fun m -> m "Considered type system:\n%t@." (TypeSystem.TypeAut.print ts));
      failwith "nothing added"
    end;
    t

  let rec find_fixpoint t =
    Log.info (fun m -> m "Searching new fixpoint...");
    (* let t = grow t in
       let t = grow t in *)
    (* let t = grow t in *)
    let t = complete t in
    Log.debug (fun m -> m "\nAutomaton after completion:\n%t@." (Aut.print t.aut));
    (* debug "COMPLETED.\n@."; *)
    begin match solve t with
      | Solver.Sat solved_aut ->
        Log.info (fun m -> m "Checking solution...");
        let module TypeAut2Solved = Automaton.Ext (TypeAut) (SolvedAut) in

        let solved_type_system = TypeAut2Solved.map
            (function _ -> ())
            (function () -> ())
            (function q -> Regular.Old q)
            (TypeSystem.refined_automaton t.type_system)
        in
        let extended_solved_aut = SolvedAut.merge solved_aut solved_type_system in

        let solved_type_knowledge = TypeAut2Solved.map
            (function _ -> ())
            (function () -> ())
            (function q -> Regular.Old q)
            (TypeSystem.refined_knowledge_automaton t.type_system)
        in
        let full_solved_aut = SolvedAut.merge solved_aut solved_type_knowledge in

        begin match missing_conf t full_solved_aut full_solved_aut with
          | Some (Some sample) ->
            Log.debug (fun m -> m "Invalid solution: not complete.");
            (* debug "\nINVALID Solution:\n%t\n@." (SolvedAut.print solved_aut);
               debug "NOT COMPLETE\n@."; *)
            (* let t = grow t in *)
            (* let t = grow t in *)
            find_fixpoint { (add_sample sample t) with iteration = t.iteration + 1 }
          | Some None ->
            let t = grow t in
            find_fixpoint { t with iteration = t.iteration + 1 }
          | None ->
            Log.debug (fun m -> m "Complete.");
            begin match unsolved_critical_pair_sample t full_solved_aut extended_solved_aut with
              | Some sample ->
                Log.debug (fun m -> m "Invalid solution: not R-closed:\n%t" (SolvedAut.BoundTerm.print sample));
                (* Log.debug (fun m -> m "solution:\n%t" (SolvedAut.print solved_aut)); *)
                (* debug "\nINVALID Solution:\n%t\n@." (SolvedAut.print solved_aut); *)
                Log.debug (fun m -> m "Not R-closed because of %t\n" (SolvedAut.BoundTerm.print sample));
                find_fixpoint { (add_sample sample t) with iteration = t.iteration + 1 }
              | None ->
                Log.debug (fun m -> m "Closed.");
                (* Log.debug (fun m -> m "solution:\n%t" (SolvedAut.print solved_aut)); *)
                Some solved_aut
            end
        end
      | Solver.Unsat ->
        Log.debug (fun m -> m "Unsat!");
        None
      | _ ->
        Log.err (fun m -> m "Unknown solution");
        None
    end

  let create type_system abs_typed_trs =
    let seeds = AbsTypedTrs.fold (
        fun (lhs, _) seeds ->
          begin match lhs with
            | AbsTypedPattern.Cons (f, l), (abs_ty, _) ->
              (* Log.debug (fun m -> m "SEED %t\n@." (AbsTypedPattern.print lhs)); *)
              let seed : Seed.t = (f, List.map type_of l, abs_ty) in
              SeedSet.add seed seeds
            | _ -> raise (Invalid_argument "Invalid TRS: cannot have just a variable on the left-hand-side.")
          end
      ) abs_typed_trs SeedSet.empty
    in
    let abs_type_system = Type2Abs.map (function _ -> ()) (function () -> ()) (function ty -> (Regular.Unknown, ty)) (TypeSystem.automaton type_system) in
    (* Log.debug (fun m -> m "abs aut\n%t@." (AbsAut.print abs_type_system)); *)
    let aut = Aut.create {
        Aut.automaton = abs_type_system;
        state_type = Node.abs_ty;
        normalizer = function ty -> Node.create_from_abstract ty, ()
      }
    in
    let aut, new_transitions = SeedSet.fold (
        fun (f, l, ty) (aut, _) ->
          Aut.handle (f, l, ty) aut
      ) seeds (aut, [])
    in
    let trs = stripped_trs abs_typed_trs in
    let t = {
      type_system = type_system;
      trs = trs;
      abs_typed_trs = abs_typed_trs;
      trs_patterns = Matching.pattern_automaton (StrippedTrs.left_sides trs);
      aut = aut;
      seeds = seeds;
      dirty = false;
      seed_nodes = NodeSet.empty;
      solver = Solver.create ();
      iteration = 0
    } in
    List.fold_right register_transition new_transitions t

  let find_fixpoint type_system abs_typed_trs =
    (* debug "Fixpoint with completion algorithm.@."; *)
    Log.debug (fun m -> m "\nConsidered rewriting system:\n%t\n" (AbsTypedTrs.print abs_typed_trs));
    Log.debug (fun m -> m "\nConsidered type system:\n%t\n" (TypeSystem.TypeAut.print (TypeSystem.automaton type_system)));
    let t = create type_system abs_typed_trs in
    Log.debug (fun m -> m "Initial automaton\n%t" (Aut.print t.aut));
    let t = grow t in
    (* let t = grow t in
       let t = grow t in *)
    Log.debug (fun m -> m "Initial grown automaton\n%t" (Aut.print t.aut));
    let t = find_fixpoint t in
    Log.info (fun m -> m "Fixpoint found!");
    t
end
