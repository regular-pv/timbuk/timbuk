(** Timbuk Fixpoint Learning

    Provides an invariant learning procedure based on
    the Tree Automata Completion algorithm and SMT-solving.
*)

module Completion = Completion
