open Collections
open Timbuk
open TimbukSolving
open TimbukTyping

module Make
    (Location : sig type t end)
    (TypeSystem : RefinedTypeSystem.S)
    (RegularTypePartition : Set.S with type elt = TypeSystem.TypeAut.StateSet.t)
    (AbsType : Automaton.TYPE with type t = (TypeSystem.TypeAut.State.t, RegularTypePartition.t) Regular.abs_type)
    (AbsTypedPattern : TypedPattern.S with type Sym.t = TypeSystem.TypeAut.Sym.t and type Type.t = AbsType.t * Location.t)
    (AbsTypedTrs : Relation.S with type ord = AbsTypedPattern.t)
    (SolvedAut : Automaton.S with type Sym.t = TypeSystem.TypeAut.Sym.t and type State.t = (TypeSystem.TypeAut.State.t, RegularTypePartition.t) Regular.solved_state and type Label.t = unit and type data = unit)
    (Solver : Solver.S with type Var.t = int) :
sig
  exception IrreducibleNonValue of TypeSystem.TypeAut.Sym.t Term.term

  val find_fixpoint : TypeSystem.t -> AbsTypedTrs.t -> SolvedAut.t option
end
