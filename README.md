# Timbuk 4

Timbuk is a regular verification framework based on Tree Automata and
Term Rewriting Systems.
It provides a set of libraries to create and manipulate Tree Automata and TRSs,
make reachability analysis using the Completion Algorithm,
and compute abstractions able to verify safety properties over higher-order
functional programs.

## Installation

We only support Unix systems (GNU/Linux, macOs, etc.).

### Download

Clone the GIT repository from Inria's GitLab:

```
$ git clone https://gitlab.inria.fr/regular-pv/timbuk/timbuk.git
```

### Dependencies

Timbuk 4 is written in OCaml. It has been tested from OCaml 4.08 to 4.10.

Note that for [Nix](https://nixos.org/) users,
we provide a `shell.nix` file installing most of the dependencies automatically,
the only needed step is the call to `./install-deps.sh`.
For non-Nix users, you will have to do it manually following this section.

You can install most of the needed libraries using Opam:

```
$ opam install ocamlfind logs fmt
```

You will also need to build and install the two following libraries:

  - https://github.com/timothee-haudebourg/ocaml-collections (adds some usefull data structures)
  - https://github.com/timothee-haudebourg/ocaml-process (simplifies sub-processes handling)
  - https://github.com/timothee-haudebourg/ocaml-clap (command line argument parsing)
  - https://github.com/timothee-haudebourg/ocaml-unicode (unicode charset support)
  - https://github.com/timothee-haudebourg/ocaml-code-map (code mapping, parsing utility)

This can be done in one line using the provided script:

```
$ ./install-deps.sh
```

#### SMT-solver

Timbuk 4 needs a SMT solver to run properly.
For now, only CVC4 (1.6+) is supported.

- NixOS

    A `shell.nix` file is present at the root to build an environnement with all the dependencies.
    Just type:
    ```
    $ nix-shell
    ```

- Archlinux (pacman)

    Archlinux provides a community package for CVC4. Just type:
    ```
    $ pacman -S cvc4
    ```

- Mac OS (homebrew)

    I'm not sure if CVC4 appears in the default repository sources.
    However there is an official [homebrew tap](https://github.com/CVC4/homebrew-cvc4) from which you can install CVC4 by typing:
    ```
    $ brew tap cvc4/cvc4
    $ brew install cvc4/cvc4/cvc4
    ```

### Build

Timbuk 4 is composed of many OCaml libraries in the `libs` directory,
and a command line interface located in the `bin` directory.

You can build all at once using the `Makefile` at the root.

```
$ make
```

## Usage

### Command line interface (CLI)

```
$ cd bin
$ ./timbuk my_timbuk_file.timbuk
```

You can get a little bit of help by typing:

```
$ ./timbuk --help
```

Visit the [Timbuk 4 Wiki pages](https://gitlab.inria.fr/regular-pv/timbuk/timbuk/-/wikis/home)
to have a description of the specification file format and
understand the output of the command line interface.

### Project organisation

Timbuk is made of multiples OCaml libraries and a command line interface.
Here is an overview of the source code organisation.

  - `libs`: The libraries.
    - `core`: Terms, patterns, tree automata, types, etc.
    - `completion`: Matching algorithm, completion algorithm.
    - `solving`: Finite model finder with SMT-solvers.
    - `sampling`: Regular language sampling.
    - `typing`: Regular-type-based verification.
    - `fixpoint-learning`: Fixpoint learning algorithms.
    - `spec`: Timbuk specification files handling.
  - `bin`: The command line interface.
  - `tests`: Many timbuk files.
