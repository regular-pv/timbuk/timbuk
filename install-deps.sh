#!/bin/sh
# Check dependencies
if ! opam --version 2> /dev/null 1> /dev/null ; then
	echo "OPAM is not installed.";
	exit 1
fi
if ! ocaml -version 2> /dev/null 1> /dev/null ; then
	echo "OCaml is not installed.";
	exit 1
fi
if ! dune --version 2> /dev/null 1> /dev/null ; then
	echo "dune is not installed.";
	exit 1
fi

# Move in temporary directory
rm -rf /tmp/timbuk
mkdir /tmp/timbuk
cd /tmp/timbuk

# Clone every dependencies
git clone https://github.com/timothee-haudebourg/ocaml-collections.git
git clone https://github.com/timothee-haudebourg/ocaml-process.git
git clone https://github.com/timothee-haudebourg/ocaml-clap.git
git clone https://github.com/timothee-haudebourg/ocaml-unicode.git
git clone https://github.com/timothee-haudebourg/ocaml-code-map.git

# Build/install collections
cd ocaml-collections
make
make install

# Build/install process
cd ../ocaml-process
make
make install

# Build/install clap
cd ../ocaml-clap
make
make install

# Build/install unicode
cd ../ocaml-unicode
make
make install

# Build/install code-map
cd ../ocaml-code-map
make
make install

rm -rf /tmp/timbuk
