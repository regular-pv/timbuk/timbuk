# What's next

## Subtyping

 - Implement correct subtyping in the completion based fixpoint learner.
  In the case where a rewriting rule gives not the same type for a variable
  in the left and right hand side, I don't know what happens.
  One way to do it properly would be to add epsilon transitions while
  rewriting to clearly define the type cast of the term from left to right.

 - Remove unused symbols from the considered abstraction.

## Optimisations

  - Incremental learning.
  For now, the completion based fixpoint learner rebuild the whole SMT instance
  at each iteration. It is possible to build and increment the SMT instance
  instead. This should significantly reduce the running time.

  - Partition subtyping.
  For a partition of a type t, it is sometimes needed to fine the same partition
  but for a subtype of t. This is done through the
  [refinedTypeSystem.subtype_partition] function.
  For now, this is recomputed at every call. It would be nice to memorize
  already computed sub-partition, since it involve doing many expensive automata
  intersections.

## Error reports

	- detect that every state in TC is either a final state or a substate,
	  but not both.

	- verify that every transition in TC is normalized.
