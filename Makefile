all: byte native

byte:
	make -C libs byte
	make -C bin byte
	make -C bin self_contained_lib   

native:
	make -C libs native
	make -C bin native

install:
	make -C bin install

uninstall:
	make -C bin uninstall

doc:
	make -C libs doc

clean:
	make -C libs clean
	make -C bin clean

mrproper:
	make -C libs mrproper
	make -C bin mrproper

ifneq ($(TIMBUK_ONLINE_DOC_PATH),)
publish_doc:
	scp -r doc/* $(TIMBUK_ONLINE_DOC_PATH)
	make -C libs publish_doc
endif

.PHONY: byte native libs bin doc publish_doc install clean mrproper
